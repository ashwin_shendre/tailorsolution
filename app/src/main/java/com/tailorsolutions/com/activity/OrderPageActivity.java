package com.tailorsolutions.com.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.EmployeeAdapter;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Bill;
import com.tailorsolutions.com.utils.BillViewModel;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.ItemMasterViewModel;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.MeasurementRepository;
import com.tailorsolutions.com.utils.MeasurementViewModel;
import com.tailorsolutions.com.utils.OrderItemData;
import com.tailorsolutions.com.utils.OrderVIewModel;
import com.tailorsolutions.com.utils.Orders;

import java.util.List;

public class OrderPageActivity extends AppCompatActivity {

    Button btn_check;
    OrderVIewModel orderrepository;
    MeasurementViewModel measurementrepository;
    BillViewModel billrepository;
    EditText edt_amt,edt_paid,edt_payable;
    int measurement_id=0,bill_id=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_page);
        ItemMasterViewModel repository;
        edt_amt = (EditText) findViewById(R.id.edt_total);
        edt_paid = (EditText) findViewById(R.id.edt_paid);
        edt_payable = (EditText) findViewById(R.id.edt_payabel);
        final ListView lst_item = (ListView)  findViewById(R.id.lst_items);
        final EmployeeAdapter adapter = new EmployeeAdapter(getApplicationContext(),getSupportFragmentManager());
        adapter.setData(OrderItemData.data);
        btn_check = (Button) findViewById(R.id.btn_find);
        orderrepository = ViewModelProviders.of(this).get(OrderVIewModel.class);
        measurementrepository = ViewModelProviders.of(this).get(MeasurementViewModel.class);
        billrepository = ViewModelProviders.of(this).get(BillViewModel.class);

        measurementrepository.getAll().observe(this, new Observer<List<Measurement>>() {
            @Override
            public void onChanged(@Nullable List<Measurement> measurements) {
                measurement_id = measurements.size();
            }
        });
        billrepository.getAll().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                bill_id = bills.size();
            }
        });
        orderrepository.getAll(OrderItemData.cid).observe(this, new Observer<List<Orders>>() {
            @Override
            public void onChanged(@Nullable List<Orders> orders) {

            }
        });
        orderrepository.getAll().observe(this, new Observer<List<Orders>>() {
            @Override
            public void onChanged(@Nullable List<Orders> orders) {

            }
        });
        repository = ViewModelProviders.of(this).get(ItemMasterViewModel.class);
        repository.getAll().observe(this, new Observer<List<ItemMaster>>() {
            @Override
            public void onChanged(@Nullable List<ItemMaster> itemMasters) {
                //adapter.setData(itemMasters);
                if (itemMasters.size()>0) {
                    OrderItemData.data.clear();
                    for (int i = 0; i < itemMasters.size(); i++) {
                        OrderItemData dt = new OrderItemData();
                        dt.setItem(itemMasters.get(i));
                        OrderItemData.data.add(dt);
                    }
                    adapter.setData(OrderItemData.data);
                    lst_item.setAdapter(adapter);
                }
            }
        });
        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                billrepository.insert(new Bill(edt_amt.getText().toString(),edt_paid.getText().toString(),edt_payable.getText().toString(),OrderItemData.cid));
                bill_id++;
                List<OrderItemData> data = adapter.getBox();
                for (OrderItemData d:data){
                    String iname = d.getItem().getIname();
                    if (d.getItem().getItype().equalsIgnoreCase("upper")){
                        SharedPreferences spf = getApplicationContext().getSharedPreferences(iname, Context.MODE_PRIVATE);
                        //String inm = spf.getString("upper",null);
                        if (spf!=null) {
                            String utype = spf.getString("upper", null);
                            if (utype != null && utype.equalsIgnoreCase( iname)) {

                                String len=spf.getString("len", null);
                                String chest=spf.getString("chest", null);
                                String stomach=spf.getString("stomach", null);
                                String hip=spf.getString("hip", null);
                                String shoulder=spf.getString("shoulder", null);
                                String slen=spf.getString("slen", null);
                                String neck=spf.getString("neck", null);
                                String arm=spf.getString("arm", null);
   //                             Measurement mes = new Measurement(len,"-","-","-","-","-","-","-",chest,stomach,hip,shoulder,slen,arm,neck,"some","-","Pending","some","some","1",OrderItemData.cid,"lower");
   //                             measurementrepository.insert(mes);
                                measurement_id++;
                                orderrepository.insert(new Orders(""+d.getItem().getId(),d.getItem().getIname(),"2",OrderItemData.cid,""+measurement_id,""+bill_id,"tailor",edt_amt.getText().toString()));
                            }
                        }

                    }
                    else if (d.getItem().getItype().equalsIgnoreCase("lower")){
                        SharedPreferences spf = getApplicationContext().getSharedPreferences(iname, Context.MODE_PRIVATE);

                        if (spf!=null){
                            String utype =spf.getString("lower",null);
                            if (utype!=null && utype.equalsIgnoreCase(iname)) {
                                String len=spf.getString("length", null);
                                String waist=spf.getString("waist", null);
                                String hip=spf.getString("hip", null);
                                String thie=spf.getString("thie", null);
                                String knee=spf.getString("knee", null);
                                String bottom=spf.getString("bottom", null);
                                String folk=spf.getString("folk", null);
                                String feta=spf.getString("feta", null);
                                String mouzari=spf.getString("mouzari", null);
//                                Measurement mes = new Measurement(len,waist,hip,thie,knee,knee,bottom,folk,"-","-","-","-","-","-","-","-","-","pending","some","some","1",OrderItemData.cid,"lower");
 //                               measurementrepository.insert(mes);
                                measurement_id++;
                                orderrepository.insert(new Orders(""+d.getItem().getId(),d.getItem().getIname(),"2",OrderItemData.cid,""+measurement_id,""+bill_id,"tailor",edt_amt.getText().toString()));
                            }
                        }
                    }


                }
                Toast.makeText(getApplicationContext(),"Order Saved",Toast.LENGTH_LONG).show();
            }
        });
    }
}
