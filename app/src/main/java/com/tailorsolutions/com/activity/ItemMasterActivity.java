package com.tailorsolutions.com.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import com.tailorsolutions.com.adapter.ItemMasterAdapter;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.ItemMasterViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ItemMasterActivity extends AppCompatActivity {

    Unbinder obj;

    @BindView(R.id.lst_emp)
    RecyclerView rclist;
    /**


     @BindView(R.id.etCustomerName)
     EditText edt_nm;
     */
    int ITEM_CODE=1;
    @BindView(R.id.tvcreateitem)
    TextView txt_create;
    ItemMasterViewModel repository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_master);
        obj = ButterKnife.bind(this);
        rclist.setLayoutManager(new LinearLayoutManager(this));
        rclist.setHasFixedSize(true);
        final ItemMasterAdapter adapter = new ItemMasterAdapter();
        rclist.setAdapter(adapter);
        repository = ViewModelProviders.of(this).get(ItemMasterViewModel.class);
        repository.getAll().observe(this, new Observer<List<ItemMaster>>() {
            @Override
            public void onChanged(@Nullable List<ItemMaster> itemMasters) {
                adapter.setData(itemMasters);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obj.unbind();
    }

    @OnClick(R.id.tvcreateitem)
    public void createItem(){
        Intent itemActivity = new Intent(getApplicationContext(),AddItemMasterActivity.class);
        startActivityForResult(itemActivity,ITEM_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==ITEM_CODE && resultCode==RESULT_OK){
            String itemnm = data.getStringExtra("iname");
            String itype = data.getStringExtra("itype");
            String ispec = data.getStringExtra("ispec");
            ItemMaster master = new ItemMaster(itemnm,itype,ispec);
            repository.insert(master);
        }
    }
}
