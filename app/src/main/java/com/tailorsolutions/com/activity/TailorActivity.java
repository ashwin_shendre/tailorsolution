package com.tailorsolutions.com.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.adapter.EmployeeListAdapter;
import com.tailorsolutions.com.utils.EmployeeViewModel;
import com.tailorsolutions.com.utils.Salesman;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TailorActivity extends AppCompatActivity {

    Unbinder unbinder;
    @BindView(R.id.frm_layout)
    FrameLayout frm_layout;
    @BindView(R.id.tvcreatetailor)
    TextView tvCreateTailor;
    @BindView(R.id.lst_emp)
    RecyclerView lst_emp;
    EmployeeViewModel repository;
    int tailorActivity=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tailor);
        unbinder = ButterKnife.bind(this);
        lst_emp = (RecyclerView) findViewById(R.id.lst_emp);
        lst_emp.setLayoutManager(new LinearLayoutManager(this));
        lst_emp.setHasFixedSize(true);
        final EmployeeListAdapter adapter = new EmployeeListAdapter();
        lst_emp.setAdapter(adapter);
        repository = ViewModelProviders.of(this).get(EmployeeViewModel.class);
        repository.getTailor().observe(this, new Observer<List<Salesman>>() {
            @Override
            public void onChanged(@Nullable List<Salesman> salesmen) {
                adapter.setEmp(salesmen);
            }
        });
    }
    @OnClick(R.id.tvcreatetailor)
    public void onCreateTailor(View view){
        Intent intent = new Intent(this,AddTailorActivity.class);
        startActivityForResult(intent,tailorActivity);
        /*FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_layout,new AddTailorFragment());
        transaction.addToBackStack(null);
        transaction.commit();*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==tailorActivity&&resultCode==RESULT_OK){
            String unm = data.getStringExtra("tnm");
            String mob = data.getStringExtra("mobile");
            String add = data.getStringExtra("add");
            String em = data.getStringExtra("title");
            new PopulateAsync(repository).execute(new Salesman(unm,add, mob,"12/06/98",em,"active","tailor"));
        }
    }
    private static class PopulateAsync extends AsyncTask<Salesman,Void,Void> {
        EmployeeViewModel employeeDao;

        private PopulateAsync(EmployeeViewModel employeeDao){
            this.employeeDao = employeeDao;
        }

        @Override
        protected Void doInBackground(Salesman... salesmen) {
            employeeDao.insert(salesmen[0]);
            return null;
        }
    }
}
