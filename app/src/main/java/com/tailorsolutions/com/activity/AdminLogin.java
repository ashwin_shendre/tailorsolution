package com.tailorsolutions.com.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.common.SharedPreferenceDb;
import com.tailorsolutions.com.tailoringsolutions.MainActivity;
import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminLogin extends AppCompatActivity {

    private Unbinder unbinder;
    @BindView(R.id.etName)
    EditText etUserName;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvlogin)
    TextView tvLogin;
    @BindView(R.id.tvregister)
    TextView tvRegister;
    private SharedPreferences spf;
    private String name;
    private String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        spf = SharedPreferenceDb.getInstance(getApplicationContext());
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @OnClick(R.id.tvlogin)
    public void onButtonClick(View view) {
        String name = etUserName.getText().toString();
        String password = etPassword.getText().toString();
        if(name.isEmpty()||password.isEmpty()){
            Toast.makeText(this, "Name and Password can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (name.equals(spf.getString(SharedPreferenceDb.KEY_NAME, null)) &&
                password.equals(spf.getString(SharedPreferenceDb.KEY_PASSWORD, null))) {
            Toast.makeText(getApplicationContext(), "Login Success!", Toast.LENGTH_SHORT).show();
           startActivity(new Intent(this,MainActivity.class));
           finish();

        } else {
            Toast.makeText(getApplicationContext(), "Incorrect User name and Password!", Toast.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.tvregister)
    public void onRegsiter(View view) {
     //   loadFragment(new AdminRegistrationFragment());
        Intent intent = new Intent(this,AdminRegister.class);
        startActivity(intent);

    }

}
