package com.tailorsolutions.com.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tailorsolutions.com.adapter.CustomerListAdapter;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.CustomerRepository;
import com.tailorsolutions.com.utils.CustomerViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CustomerActivity extends AppCompatActivity {

    Unbinder unbinder;
    @BindView(R.id.tvcreatecustomer)
    TextView tvCreateCustomer;
    @BindView(R.id.lst_emp)
    RecyclerView lst_emp;
    CustomerViewModel repository;
    int cutId=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        unbinder = ButterKnife.bind(this);
        lst_emp.setLayoutManager(new LinearLayoutManager(this));
        lst_emp.setHasFixedSize(true);
        final CustomerListAdapter adapter = new CustomerListAdapter();

        repository = ViewModelProviders.of(this).get(CustomerViewModel.class);
        repository.getAll().observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(@Nullable List<Customer> customers) {
                adapter.setCust(customers);
                adapter.notifyDataSetChanged();
                lst_emp.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @OnClick(R.id.tvcreatecustomer)
    public void onCreateCuctomer(View view){
        Intent intent = new Intent(this,AddCustomerActivity.class);
        startActivityForResult(intent,cutId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==cutId&&resultCode==RESULT_OK){
            String cnm = data.getStringExtra("cnm");
            String cmb = data.getStringExtra("cmob");
            String cem = data.getStringExtra("cem");
            String add = data.getStringExtra("add");
            Customer c = new Customer(cnm,cmb,cem,add);
            new PopulateCustomer(repository).execute(c);
        }
    }
    private static class PopulateCustomer extends AsyncTask<Customer,Void,Void>{

        private  CustomerViewModel customerViewModel;
        public PopulateCustomer(CustomerViewModel customerViewModel){
            this.customerViewModel = customerViewModel;
        }
        @Override
        protected Void doInBackground(Customer... customers) {
            customerViewModel.insert(customers[0]);
            return null;
        }
    }
}
