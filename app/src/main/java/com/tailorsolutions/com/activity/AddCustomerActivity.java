package com.tailorsolutions.com.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddCustomerActivity extends AppCompatActivity {

    Unbinder obj;
    @BindView(R.id.etCustomerName)
    EditText edt_nm;
    @BindView(R.id.etCustomerMobile)
    EditText edt_mob;
    @BindView(R.id.etemail)
    EditText edt_em;
    @BindView(R.id.etaddress)
    EditText edt_address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        obj = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obj.unbind();
    }
    @OnClick(R.id.tvCustomerRegister)
    public void custReg(){
        Intent data = new Intent();
        data.putExtra("cnm",edt_nm.getText().toString());
        data.putExtra("cmob",edt_mob.getText().toString());
        data.putExtra("cem",edt_em.getText().toString());
        data.putExtra("add",edt_address.getText().toString());
        setResult(RESULT_OK,data);
        finish();
    }
}
