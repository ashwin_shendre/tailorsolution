package com.tailorsolutions.com.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddItemMasterActivity extends AppCompatActivity {

    Unbinder obj;

    @BindView(R.id.etItemName)
    EditText edt_inm;

    @BindView(R.id.etItemspec)
    EditText edt_ispc;

    @BindView(R.id.spitemtype)
    Spinner sp_itype;
    String[] itype = {"Upper","Lower","Accessories"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item_master);
        obj = ButterKnife.bind(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item,itype);
        sp_itype.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obj.unbind();
    }

    @OnClick(R.id.tvSalesmanRegister)
    public void icreate()
    {
        Intent data = new Intent();
        data.putExtra("iname",edt_inm.getText().toString());
        data.putExtra("itype",itype[sp_itype.getSelectedItemPosition()]);
        data.putExtra("ispec",edt_ispc.getText().toString());
        setResult(RESULT_OK,data);
        finish();
    }
}
