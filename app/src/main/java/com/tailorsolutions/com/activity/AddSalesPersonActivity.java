package com.tailorsolutions.com.activity;

import android.content.Intent;
import android.os.Binder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddSalesPersonActivity extends AppCompatActivity {

    Unbinder obj;

    @BindView(R.id.etSalesmanName)
    EditText edt_unm;
    @BindView(R.id.etSalesmanMobile)
    EditText edt_mob;
    @BindView(R.id.etSalesmanAddress)
    EditText edt_address;
    @BindView(R.id.etemail)
    EditText edt_em;
    @BindView(R.id.tvSalesmanRegister)
    TextView txt_add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sales_person);
        obj = ButterKnife.bind(this);
    }
    @OnClick(R.id.tvSalesmanRegister)
    public void sendData(){
        Intent data = new Intent();
        data.putExtra("unm",edt_unm.getText().toString());
        data.putExtra("mobile",edt_mob.getText().toString());
        data.putExtra("add",edt_address.getText().toString());
        data.putExtra("em",edt_em.getText().toString());
        setResult(RESULT_OK,data);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obj.unbind();
    }
}
