package com.tailorsolutions.com.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddTailorActivity extends AppCompatActivity {

    Unbinder obj;
    @BindView(R.id.etTailorName)
    EditText edt_tnm;
    @BindView(R.id.etTailorMobile)
    EditText edt_mob;
    @BindView(R.id.etTailorAddress)
    EditText edt_add;
    @BindView(R.id.etTailorTitle)
    EditText edt_title;

    @BindView(R.id.tvTailorRegister)
    TextView txt_reg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tailor);
        obj = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        obj.unbind();
    }

    @OnClick(R.id.tvTailorRegister)
    public void tailorRegister(){
        Intent data = new Intent();
        data.putExtra("tnm",edt_tnm.getText().toString());
        data.putExtra("mob",edt_mob.getText().toString());
        data.putExtra("add",edt_add.getText().toString());
        data.putExtra("title",edt_title.getText().toString());
        setResult(RESULT_OK,data);
        finish();
    }
}
