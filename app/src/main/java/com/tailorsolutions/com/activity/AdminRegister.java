package com.tailorsolutions.com.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.common.SharedPreferenceDb;
import com.tailorsolutions.com.tailoringsolutions.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AdminRegister extends AppCompatActivity {

    Unbinder unbinder;
    @BindView(R.id.etRegisterName)
    EditText etRegisterName;
    @BindView(R.id.etRegisterPassword)
    EditText etRegisterPassword;
    @BindView(R.id.etRegisterComfirmPassword)
    EditText etConfirmedPassword;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    private String name;
    private String password;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register);
        unbinder = ButterKnife.bind(this);
        context = getApplicationContext();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @OnClick(R.id.tvRegister)
    public void onRegister(View view){
        name = etRegisterName.getText().toString();
        password = etRegisterPassword.getText().toString();
        String confirmPass = etConfirmedPassword.getText().toString();
        if(name.isEmpty()|| password.isEmpty()|| confirmPass.isEmpty()){
            Toast.makeText(context, "Name and Password can't be empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.equals(confirmPass)){
            saveData();
        }
        else{
            Toast.makeText(context, "Password doesn't match with Confirm Password! ", Toast.LENGTH_SHORT).show();
        }

    }
    public void saveData(){
        SharedPreferences spf = SharedPreferenceDb.getInstance(context);
        SharedPreferences.Editor editor = spf.edit();
        editor.putString(SharedPreferenceDb.KEY_NAME,name);
        editor.putString(SharedPreferenceDb.KEY_PASSWORD,password);
        editor.commit();
        Toast.makeText(context, "User Created!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,AdminLogin.class);
        startActivity(intent);
        finish();
    }
}
