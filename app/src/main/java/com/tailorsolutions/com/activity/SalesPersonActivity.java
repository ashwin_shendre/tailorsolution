package com.tailorsolutions.com.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.adapter.EmployeeAdapter;
import com.tailorsolutions.com.adapter.EmployeeListAdapter;
import com.tailorsolutions.com.utils.EmployeeViewModel;
import com.tailorsolutions.com.utils.Salesman;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SalesPersonActivity extends AppCompatActivity {
    Unbinder unbinder;
    @BindView(R.id.tvcreatesalespersons)
    TextView tvCreateSalesMan;
    //@BindView(R.id.lst_emp)
    RecyclerView lst_emp;
    public static List<Salesman> lstemp=new ArrayList<>();
    EmployeeAdapter adapter;
    EmployeeViewModel repository;
    int Salesid=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_person);
        unbinder = ButterKnife.bind(this);
        lst_emp = (RecyclerView) findViewById(R.id.lst_emp);
        lst_emp.setLayoutManager(new LinearLayoutManager(this));
        lst_emp.setHasFixedSize(true);
        final EmployeeListAdapter adapter = new EmployeeListAdapter();
        lst_emp.setAdapter(adapter);
        repository = ViewModelProviders.of(this).get(EmployeeViewModel.class);
        repository.getSalesman().observe(this, new Observer<List<Salesman>>() {
            @Override
            public void onChanged(@Nullable List<Salesman> salesmen) {
                //repository.insert(new Salesman("xyz","solapur", "1234567890","12/06/98","abc@abc.com","active","salesman"));
                //repository.insert(new Salesman("abc","solapur", "1234567890","12/06/98","abc@abc.com","active","salesman"));
                adapter.setEmp(salesmen);

            }
        });
        //new PopulateAsync(repository).execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @OnClick(R.id.tvcreatesalespersons)
    public void onCreateSalesMan(View view){
        Intent intent = new Intent(this,AddSalesPersonActivity.class);
        startActivityForResult(intent,Salesid);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Salesid && resultCode==RESULT_OK){
            String unm = data.getStringExtra("unm");
            String mob = data.getStringExtra("mobile");
            String add = data.getStringExtra("add");
            String em = data.getStringExtra("em");
            //Salesman sls = new Salesman(unm,add, mob,"12/06/98",em,"active","salesman");
            repository.insert(new Salesman(unm,add, mob,"12/06/98",em,"active","salesman"));
            //new PopulateAsync(repository).execute(sls);
        }
    }

    private static class PopulateAsync extends AsyncTask<Salesman,Void,Void> {
        EmployeeViewModel employeeDao;

        private PopulateAsync(EmployeeViewModel employeeDao){
            this.employeeDao = employeeDao;
        }

        @Override
        protected Void doInBackground(Salesman... salesmen) {
            //employeeDao.insert(new Salesman("abc","solapur", "1234567890","12/06/98","abc@abc.com","active","salesman"));
            employeeDao.insert(salesmen[0]);
            return null;
        }
    }
}
