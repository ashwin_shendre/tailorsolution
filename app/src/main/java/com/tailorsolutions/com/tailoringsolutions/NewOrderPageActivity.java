package com.tailorsolutions.com.tailoringsolutions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tailorsolutions.com.adapter.BillPagerAdapter;
import com.tailorsolutions.com.adapter.ItemAddAdapter;
import com.tailorsolutions.com.fragments.UpperMeasurementFragment;
import com.tailorsolutions.com.utils.Bill;
import com.tailorsolutions.com.utils.BillViewModel;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.ItemMasterViewModel;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.MeasurementViewModel;
import com.tailorsolutions.com.utils.OrderItemData;
import com.tailorsolutions.com.utils.OrderVIewModel;
import com.tailorsolutions.com.utils.Orders;

import java.util.ArrayList;
import java.util.List;

public class NewOrderPageActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabs;
    static List<ItemMaster> itemdata=new ArrayList<>();
    public static ItemMasterViewModel repository;
    public static MeasurementViewModel measurementrepository;
    public static String[] title={"Menu","Upper","Lower"};
    public static int measurementid=0,bill_id;
    public static OrderVIewModel orderrepository;
    public static BillViewModel billrepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_page);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        BillPagerAdapter adapter = new BillPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
        //OrderItemData.udata.clear();
        //OrderItemData.ldata.clear();
        billrepository = ViewModelProviders.of(this).get(BillViewModel.class);
        billrepository.getAll().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                bill_id = bills.size()+1;
            }
        });
        measurementrepository = ViewModelProviders.of(this).get(MeasurementViewModel.class);
        measurementrepository.getAll().observe(this, new Observer<List<Measurement>>() {
            @Override
            public void onChanged(@Nullable List<Measurement> measurements) {
                measurementid = measurements.size();
            }
        });
        repository = ViewModelProviders.of(this).get(ItemMasterViewModel.class);
        repository.getAll().observe(this, new Observer<List<ItemMaster>>() {
            @Override
            public void onChanged(@Nullable List<ItemMaster> itemMasters) {
                OrderItemData.itemdata.clear();
                OrderItemData.itemdata.addAll(itemMasters);
            }
        });
        orderrepository = ViewModelProviders.of(this).get(OrderVIewModel.class);
        orderrepository.getAll().observe(this, new Observer<List<Orders>>() {
            @Override
            public void onChanged(@Nullable List<Orders> orders) {

            }
        });
    }
    public List<ItemMaster> getdata(){
        itemdata.clear();
        itemdata.addAll(OrderItemData.itemdata);
        return itemdata;
    }
}
