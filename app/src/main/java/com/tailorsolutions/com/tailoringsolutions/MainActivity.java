package com.tailorsolutions.com.tailoringsolutions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.tailorsolutions.com.activity.AdminLogin;
import com.tailorsolutions.com.activity.CustomerActivity;
import com.tailorsolutions.com.activity.ItemMasterActivity;
import com.tailorsolutions.com.activity.OrderPageActivity;
import com.tailorsolutions.com.activity.SalesPersonActivity;
import com.tailorsolutions.com.activity.TailorActivity;
import com.tailorsolutions.com.common.SharedPreferenceDb;
import com.tailorsolutions.com.fragments.HomeFragment;
import com.tailorsolutions.com.utils.OrderItemData;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private static long back_pressed;
    private SharedPreferences spf;
    private SharedPreferences.Editor editor;
    private boolean login_state;
    FragmentManager fmanager;
    FragmentTransaction transaction;
    private  Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Tailoring Solutions");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navgation_view);
        navigationView.setNavigationItemSelectedListener(this);
        TextView txt_itemMaster = (TextView) findViewById(R.id.tvitem);
       // TextView txt_customerlist = (TextView) findViewById(R.id.tvolist);
        TextView txt_order = (TextView) findViewById(R.id.tvorder);
        TextView txt_bills = (TextView) findViewById(R.id.tvbill);
        txt_itemMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itemIntent = new Intent(getApplicationContext(), ItemMasterActivity.class);
                startActivity(itemIntent);
            }
        });
        txt_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent orderIntent = new Intent(getApplicationContext(), CustomerListActivity.class);
                startActivity(orderIntent);
            }
        });
        /*txt_customerlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderItemData.ostate="measurement";
                Intent customerListIntent = new Intent(getApplicationContext(),CustomerListActivity.class);
                startActivity(customerListIntent);
            }
        });*/
        txt_bills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderItemData.ostate="bill";
                Intent customerListIntent = new Intent(getApplicationContext(),BillActivity.class);
                startActivity(customerListIntent);
            }
        });
        initData();

    }
  public void initData(){
      spf = SharedPreferenceDb.getInstance(MainActivity.this);
      editor = SharedPreferenceDb.getEditor();
      String user_name = spf.getString(SharedPreferenceDb.KEY_NAME,null);
      String user_password = spf.getString(SharedPreferenceDb.KEY_PASSWORD,null);
      if(user_name == null||user_password == null){
          login_state = false;
          startActivity(new  Intent(this,AdminLogin.class) );
      }
      else{
          login_state = true;
          //loadFragment(new HomeFragment());
      }
  }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onBackPressed() {
        finish();
       *//* if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
        else
            Toast.makeText(getBaseContext(), "Press again to close app!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();*//*
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.nav_home:
                break;
            case R.id.nav_tailor:
                intent = new Intent(this,TailorActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_sales_persons:
                intent = new Intent(this,SalesPersonActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_customers:
                intent = new Intent(this,CustomerActivity.class);
                startActivity(intent);
                break;
           /* case R.id.nav_billings:
                break;
            case R.id.nav_profile:
                break;*/
            case R.id.nav_login:
                intent = new Intent(this,AdminLogin.class);
                startActivity(intent);
                menuItem.setChecked(true);
                break;
            case R.id.nav_logout:
                //logout();

                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

  /*  public void loadFragment(Fragment fragment) {
        fmanager = getSupportFragmentManager();
        transaction = fmanager.beginTransaction();
        transaction.replace(R.id.content_layout,fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }*/
/*  public void logout(){
        editor.clear();
        editor.commit();
        login_state = false;
        navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
        navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        loadFragment(new AdminLoginFragment());
    }
*/
    @Override
    protected void onResume() {
        super.onResume();
        /*if(login_state){
            Log.i("MainActivity", "onResume: "+login_state);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
        }
        else {
            Log.i("MainActivity", "onResume: " + login_state);
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        }*/
    }
   /* @Override
    public void onBackPressed() {
        // If the fragment exists and has some back-stack entry

            // Let super handle the back press
        finish();
        }*/
}
