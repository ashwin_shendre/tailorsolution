package com.tailorsolutions.com.tailoringsolutions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;

import com.tailorsolutions.com.adapter.BillAdapter;
import com.tailorsolutions.com.adapter.OrderListAdapter;
import com.tailorsolutions.com.utils.Bill;
import com.tailorsolutions.com.utils.BillViewModel;
import com.tailorsolutions.com.utils.BillsUtils;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.CustomerRepository;
import com.tailorsolutions.com.utils.CustomerViewModel;
import com.tailorsolutions.com.utils.OrderItemData;
import com.tailorsolutions.com.utils.OrderVIewModel;
import com.tailorsolutions.com.utils.Orders;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BillActivity extends AppCompatActivity {

    Unbinder bind;
    BillViewModel repository;
    OrderVIewModel orderrepository;
    CustomerViewModel customerRepository;
    @BindView(R.id.lst_orders)
    ListView lst_orders;
    BillAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        bind = ButterKnife.bind(this);
        adapter = new BillAdapter(getApplicationContext());
        repository = ViewModelProviders.of(this).get(BillViewModel.class);
        orderrepository = ViewModelProviders.of(this).get(OrderVIewModel.class);
        customerRepository = ViewModelProviders.of(this).get(CustomerViewModel.class);
        BillsUtils.uorders.clear();
        BillsUtils.lorders.clear();
        BillsUtils.billsUtils.clear();
        repository.getAll().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                for(int i=0;i<bills.size();i++){
                    getCustomerName(bills.get(i));
                }

                adapter.setOrders(BillsUtils.billsUtils);
                lst_orders.setAdapter(adapter);
            }
        });


    }
    public void check(String cid){
        final OrderListAdapter adapter = new OrderListAdapter(getApplicationContext());
        orderrepository.getAll().observe(this, new Observer<List<Orders>>() {
            @Override
            public void onChanged(@Nullable List<Orders> orders) {
                adapter.setOrders(orders);
                BillsUtils.orderslist.addAll(orders);
                lst_orders.setAdapter(adapter);
            }
        });
    }

    void getCustomerName(final Bill bill){
        final String[] cnm = {"No"};
        customerRepository.getCustomer(Integer.parseInt(bill.getCid())).observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(@Nullable List<Customer> customers) {
                BillsUtils billsUtils  = new BillsUtils();
                billsUtils.setBill(bill);
                 billsUtils.setCname(customers.get(0).getCname());
                 BillsUtils.billsUtils.add(billsUtils);
                 adapter.setOrders(BillsUtils.billsUtils);
            }
        });
        //return cnm[0];
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }
}
