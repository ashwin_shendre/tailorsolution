package com.tailorsolutions.com.tailoringsolutions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.BillPagerAdapter;
import com.tailorsolutions.com.adapter.ViewBillPagerAdapter;
import com.tailorsolutions.com.fragments.ViewFragmentLower;
import com.tailorsolutions.com.fragments.ViewFragmentUpper;
import com.tailorsolutions.com.fragments.ViewOrderFragment;
import com.tailorsolutions.com.utils.BillViewModel;
import com.tailorsolutions.com.utils.BillsUtils;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.ItemMasterViewModel;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.MeasurementViewModel;
import com.tailorsolutions.com.utils.OrderVIewModel;
import com.tailorsolutions.com.utils.Orders;

import java.util.ArrayList;
import java.util.List;

public class ViewOrdersActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabs;
    static List<ItemMaster> itemdata=new ArrayList<>();
    public static ItemMasterViewModel repository;
    public static MeasurementViewModel measurementrepository;
    public static String[] title={"Menu","Upper","Lower"};
    public static int measurementid=0,bill_id;
    public static OrderVIewModel orderrepository;
    public static BillViewModel billrepository;
    public static List<Orders> orders = new ArrayList<>();
    public static int s=0,t;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_orders);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        ViewBillPagerAdapter adapter = new ViewBillPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
        BillsUtils.uorders.clear();
        BillsUtils.lorders.clear();
        orders.clear();
        orderrepository = ViewModelProviders.of(this).get(OrderVIewModel.class);
        measurementrepository = ViewModelProviders.of(this).get(MeasurementViewModel.class);
        orderrepository.getAll(BillsUtils.billid).observe(this, new Observer<List<Orders>>() {
            @Override
            public void onChanged(@Nullable List<Orders> orders) {
                ViewOrdersActivity.orders.addAll(orders);
                BillsUtils.orderslist.addAll(orders);
                for (int i=0;i<orders.size();i++){
                    if (orders.get(i).getMeasureid().contains(",")){
                        String[] ids=orders.get(i).getMeasureid().split(",");
                        s=ids.length;
                        t=0;
                        for (t=0;t<ids.length;t++) {
                            getMeasurements(ids[t], orders.get(i));
                        }
                    }
                    else{
                        getMeasurements(orders.get(i).getMeasureid(),orders.get(i));
                    }
                }

                try{
                    ViewOrderFragment.adapter.setOrders(orders);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Please wait loading the data",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void getMeasurements(String measureid, final Orders od) {

        measurementrepository.getAll(Integer.parseInt(measureid)).observe(this, new Observer<List<Measurement>>() {
            @Override
            public void onChanged(@Nullable List<Measurement> measurements) {
                if (measurements.get(0).getMtype().equalsIgnoreCase("upper")) {
                    BillsUtils.uorders.add(od);
                    BillsUtils.umeasurements.add(measurements.get(0));
                    ViewFragmentUpper.setData();
                }
                else if (measurements.get(0).getMtype().equalsIgnoreCase("lower")) {
                    BillsUtils.lorders.add(od);
                    BillsUtils.lmeasurements.add(measurements.get(0));
                    //ViewFragmentLower.setData();
                }
                else{
                    BillsUtils.uorders.add(od);
                    BillsUtils.lorders.add(od);
                    if (s==3){
                        switch (t){
                            case 0:BillsUtils.umeasurements.add(measurements.get(0));
                                    break;
                            case 1:BillsUtils.umeasurements.add(measurements.get(0));
                                break;
                            case 2:BillsUtils.lmeasurements.add(measurements.get(0));
                                break;
                        }
                    }
                    else if (s==2){
                        switch (t){
                            case 0:BillsUtils.umeasurements.add(measurements.get(0));
                                break;
                            case 1:BillsUtils.lmeasurements.add(measurements.get(0));
                                break;

                        }
                    }
                    //BillsUtils.tmeasurements.add(measurements.get(0));
                    ViewFragmentUpper.setData();
                }

            }
        });
    }


}
