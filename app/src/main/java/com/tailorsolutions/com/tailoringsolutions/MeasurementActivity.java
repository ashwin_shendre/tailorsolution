package com.tailorsolutions.com.tailoringsolutions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.tailorsolutions.com.fragments.LowerFragment;
import com.tailorsolutions.com.fragments.UpperFragment;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.MeasurementViewModel;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MeasurementActivity extends AppCompatActivity {

    Unbinder bind;
    @BindView(R.id.btn_upper)
    TextView txt_upper;
    public static TextView txt_status;
    MeasurementViewModel repository;
    @BindView(R.id.btn_lower)
    TextView txt_lower;
    public static List<Measurement> data=new ArrayList<>();
    public static List<Measurement> ldata=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);
        bind = ButterKnife.bind(this);
        txt_status = (TextView) findViewById(R.id.bottomview);
        repository = ViewModelProviders.of(this).get(MeasurementViewModel.class);
        repository.getAllLower(OrderItemData.cid).observe(this, new Observer<List<Measurement>>() {
            @Override
            public void onChanged(@Nullable List<Measurement> measurements) {
                data = measurements;
                //txt_lower.setText(data.get(0).getStatus());
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.cntr,new UpperFragment());
                ft.commit();
            }
        });
        repository.getAllUpper(OrderItemData.cid).observe(this, new Observer<List<Measurement>>() {
            @Override
            public void onChanged(@Nullable List<Measurement> measurements) {
                //txt_lower.setText(data.get(0).getStatus());
                ldata = measurements;
            }
        });

    }

    @OnClick(R.id.btn_lower)
    public void lower(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.cntr,new LowerFragment());
        ft.commit();
    }

    @OnClick(R.id.btn_upper)
    public void upper(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.cntr,new UpperFragment());
        ft.commit();
    }
}
