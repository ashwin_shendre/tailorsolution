package com.tailorsolutions.com.tailoringsolutions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.CustomerListBaseAdapter;
import com.tailorsolutions.com.adapter.CustomerSearchAdapter;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.CustomerViewModel;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.List;

public class CustomerListActivity extends AppCompatActivity {

    ListView lst_cust;
    CustomerViewModel repository;
    AutoCompleteTextView searchMob;
    CustomerSearchAdapter adapter;
    EditText edt_name,edt_add;
    TextView txt_click;
    int cstatus=0;
    String custid="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        searchMob = (AutoCompleteTextView) findViewById(R.id.txt_nm);
        edt_name = (EditText) findViewById(R.id.edt_cust_name);
        edt_add = (EditText) findViewById(R.id.edt_cust_address);
        txt_click = (TextView) findViewById(R.id.txt_ok);
        //lst_cust = (ListView) findViewById(R.id.lst_cust);
        //final CustomerListBaseAdapter adapter = new CustomerListBaseAdapter(getApplicationContext());
        //lst_cust.setAdapter(adapter);
        repository = ViewModelProviders.of(this).get(CustomerViewModel.class);
        repository.getAll().observe(this, new Observer<List<Customer>>() {
            @Override
            public void onChanged(@Nullable List<Customer> customers) {
                //adapter.setData(customers);
                adapter = new CustomerSearchAdapter(getApplicationContext(),R.layout.activity_customer_list,R.id.txt_mob,customers);
                searchMob.setAdapter(adapter);
                custid=""+customers.size();
            }
        });
        searchMob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Customer cust = (Customer) parent.getItemAtPosition(position);
                edt_name.setText(cust.getCname());
                edt_add.setText(cust.getCaddress());
                cstatus=1;
                custid = ""+cust.getCid();
                //Toast.makeText(getApplicationContext(),cust.getCname(),Toast.LENGTH_LONG).show();
            }
        });
        txt_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cstatus==0){
                    String tid=storeData();
                    if (tid!=null) {
                        custid = tid;
                        Toast.makeText(getApplicationContext(), "New Customer Added", Toast.LENGTH_LONG).show();
                        OrderItemData.cid = custid;
                        Intent orderIntent = new Intent(getApplicationContext(),NewOrderPageActivity.class);
                        finish();
                        startActivity(orderIntent);
                    }
                    else
                        Toast.makeText(getApplicationContext(), "Some Fields are empty", Toast.LENGTH_LONG).show();

                }
                else{
                    Toast.makeText(getApplicationContext(),custid,Toast.LENGTH_LONG).show();
                    OrderItemData.cid = custid;
                    Intent orderIntent = new Intent(getApplicationContext(),NewOrderPageActivity.class);
                    finish();
                    startActivity(orderIntent);
                }
                searchMob.setText("");
                edt_name.setText("");
                edt_add.setText("");
                cstatus=0;
            }
        });
    }

    private String storeData() {
        int cid=0;
        if (validate()){
            cid = Integer.parseInt(custid)+1;
            Toast.makeText(getApplicationContext(),"Stored data",Toast.LENGTH_LONG).show();
            Customer customer = new Customer(edt_name.getText().toString(),searchMob.getText().toString(),"NO",edt_add.getText().toString());
            repository.insert(customer);
            return ""+cid;
        }
        return null;
    }

    private boolean validate() {
        if (edt_add.getText().toString().equals("")||edt_name.getText().toString().equals("")||searchMob.getText().toString().equals(""))
            return false;
        return true;
    }
}
