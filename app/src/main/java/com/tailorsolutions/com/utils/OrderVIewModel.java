package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class OrderVIewModel extends AndroidViewModel {
    private OrderRepository repository;
    private LiveData<List<Orders>> orders;
     public OrderVIewModel(@NonNull Application application) {
        super(application);
        repository = new OrderRepository(application);
        orders = repository.getAll();
    }
    public void insert(Orders order){repository.insert(order);}
    public void update(Orders order){repository.update(order);}
    public void delete(Orders order){repository.delete(order);}
    public void deleteAll(Orders order){repository.deleteAll();}
    public LiveData<List<Orders>> getAll(){return orders;}
    public LiveData<List<Orders>> getAll(String id){return repository.getAll(id);}
}
