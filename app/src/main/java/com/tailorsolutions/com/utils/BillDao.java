package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface BillDao {

    @Insert
    void insert(Bill bill);

    @Update
    void update(Bill bill);

    @Delete
    void delete(Bill bill);

    @Query("DELETE FROM tbl_bill")
    void deleteAll();

    @Query("SELECT * FROM tbl_bill")
    LiveData<List<Bill>> getAll();

    @Query("SELECT * FROM tbl_bill where cid=:cid")
    LiveData<List<Bill>> getBill(String cid);
}
