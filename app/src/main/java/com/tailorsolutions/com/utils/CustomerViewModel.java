package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class CustomerViewModel extends AndroidViewModel {
    private CustomerRepository customerRepository;
    private LiveData<List<Customer>> customer;
    public CustomerViewModel(@NonNull Application application) {
        super(application);
        customerRepository = new CustomerRepository(application);
        customer = customerRepository.getAll();
    }
    public void insert(Customer customer){
        customerRepository.insert(customer);
    }
    public void update(Customer customer){
        customerRepository.update(customer);
    }
    public void delete(Customer customer){
        customerRepository.delete(customer);
    }
    public void deleteAll(){
        customerRepository.deleteAll();
    }
    public LiveData<List<Customer>> getAll(){
        return customer;
    }
    public LiveData<List<Customer>> getCustomer(int cid){
        return customerRepository.getCustomer(cid);
    }
}
