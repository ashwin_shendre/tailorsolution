package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_item")
public class ItemMaster {
    @PrimaryKey(autoGenerate = true)
    int id;

    String iname;
    String itype;
    String ispecification;

    public ItemMaster(String iname,String itype,String ispecification){
        this.iname = iname;
        this.itype = itype;
        this.ispecification = ispecification;
    }

    public int getId() {
        return id;
    }

    public String getIname() {
        return iname;
    }

    public String getItype() {
        return itype;
    }

    public String getIspecification() {
        return ispecification;
    }

    public void setIspecification(String ispecification) {
        this.ispecification = ispecification;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIname(String iname) {
        this.iname = iname;
    }

    public void setItype(String itype) {
        this.itype = itype;
    }
}
