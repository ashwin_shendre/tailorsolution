package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_customer")
public class Customer {
    @PrimaryKey(autoGenerate = true)
    private int cid;
    private String cname;
    private String cmobile;
    private String cemail;
    private String caddress;

    public Customer(String cname,String cmobile,String cemail,String caddress){
        this.cname = cname;
        this.cmobile = cmobile;
        this.cemail = cemail;
        this.caddress = caddress;
    }

    public int getCid() {
        return cid;
    }

    public String getCname() {
        return cname;
    }

    public String getCmobile() {
        return cmobile;
    }

    public String getCemail() {
        return cemail;
    }

    public String getCaddress() {
        return caddress;
    }


    public void setCid(int cid) {
        this.cid = cid;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public void setCmobile(String cmobile) {
        this.cmobile = cmobile;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public void setCaddress(String caddress) {
        this.caddress = caddress;
    }
}
