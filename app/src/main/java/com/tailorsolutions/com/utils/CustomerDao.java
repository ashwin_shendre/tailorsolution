package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;
@Dao
public interface CustomerDao {
    @Insert
    void insert(Customer customer);

    @Update
    void update(Customer customer);

    @Delete
    void delete(Customer customer);

    @Query("DELETE FROM tbl_customer")
    void deleteAll();

    @Query("SELECT * FROM tbl_customer")
    LiveData<List<Customer>> getAll();

    @Query("SELECT * FROM tbl_customer where cid= :cid")
    LiveData<List<Customer>> getCustomer(int cid);
}
