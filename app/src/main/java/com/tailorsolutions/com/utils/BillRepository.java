package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class BillRepository {
    BillDao billDao;
    LiveData<List<Bill>> bill;

    public BillRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);
        billDao = database.billDao();
        bill = billDao.getAll();
    }

    public void insert(Bill bill){
        new InsertAsync(billDao).execute(bill);
    }
    public void update(Bill bill){
        new UpdateAsync(billDao).execute(bill);
    }
    public void delete(Bill bill){
        new DeleteAsync(billDao).execute(bill);
    }

    public void deleteAll(){
        new DeleteAllAsync(billDao).execute();
    }
    public LiveData<List<Bill>> getAll(){
        return  bill;
    }
    public LiveData<List<Bill>> getBill(String cid){
        return  billDao.getBill(cid);
    }

    private static class InsertAsync extends AsyncTask<Bill,Void,Void> {

        private BillDao billDao;
        private InsertAsync(BillDao billDao){
            this.billDao= billDao;
        }


        @Override
        protected Void doInBackground(Bill... bills) {
            billDao.insert(bills[0]);
            return null;
        }
    }
    private static class UpdateAsync extends AsyncTask<Bill,Void,Void>{

        private BillDao billDao;
        private UpdateAsync(BillDao billDao){
            this.billDao= billDao;
        }


        @Override
        protected Void doInBackground(Bill... bills) {
            billDao.update(bills[0]);
            return null;
        }
    }
    private static class DeleteAsync extends AsyncTask<Bill,Void,Void>{

        private BillDao billDao;
        private DeleteAsync(BillDao billDao){
            this.billDao= billDao;
        }


        @Override
        protected Void doInBackground(Bill... bills) {
            billDao.delete(bills[0]);
            return null;
        }
    }

    private static class DeleteAllAsync extends AsyncTask<Void,Void,Void>{

        private BillDao billDao;
        private DeleteAllAsync(BillDao billDao){
            this.billDao= billDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            billDao.deleteAll();
            return null;
        }
    }
}
