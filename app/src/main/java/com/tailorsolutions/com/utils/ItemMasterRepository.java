package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class ItemMasterRepository {
    ItemDao itemDao;
    LiveData<List<ItemMaster>> items;

    public ItemMasterRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);

        itemDao = database.itemDao();
        items = itemDao.getAll();

    }
    public void insert(ItemMaster item){new InsertAsyncTask(itemDao).execute(item);}
    public void update(ItemMaster item){new UpdatetAsyncTask(itemDao).execute(item);}
    public void delete(ItemMaster item){new DeleteAsyncTask(itemDao).execute(item);}
    public void deleteAll(){new DeleteAllAsyncTask(itemDao).execute();}
    public LiveData<List<ItemMaster>> getAll(){return items;}


    private static class InsertAsyncTask extends AsyncTask<ItemMaster,Void,Void> {
        private ItemDao itemDao;

        private InsertAsyncTask(ItemDao itemDao){
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(ItemMaster... itemMasters) {
            itemDao.insert(itemMasters[0]);
            return null;
        }
    }
    private static class UpdatetAsyncTask extends AsyncTask<ItemMaster,Void,Void> {
        private ItemDao itemDao;

        private UpdatetAsyncTask(ItemDao itemDao){
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(ItemMaster... itemMasters) {
            itemDao.update(itemMasters[0]);
            return null;
        }
    }
    private static class DeleteAsyncTask extends AsyncTask<ItemMaster,Void,Void> {
        private ItemDao itemDao;

        private DeleteAsyncTask(ItemDao itemDao){
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(ItemMaster... itemMasters) {
            itemDao.delete(itemMasters[0]);
            return null;
        }
    }
    private static class DeleteAllAsyncTask extends AsyncTask<Void,Void,Void> {
        private ItemDao itemDao;

        private DeleteAllAsyncTask(ItemDao itemDao){
            this.itemDao = itemDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            itemDao.deleteAll();
            return null;
        }
    }
}
