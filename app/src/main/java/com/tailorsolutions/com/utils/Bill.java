package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_bill")
public class Bill {
    @PrimaryKey(autoGenerate = true)
    private int bid;
    private String bamt;
    private String bpaid;
    private String bpayable;
    private String bbalance;
    private String cid;

    public Bill(String bamt,String bpaid,String bpayable,String cid){
        this.bamt = bamt;
        this.bpaid = bpaid;
        this.bpayable = bpayable;
        this.bbalance = bpayable;
        this.cid = cid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public int getBid() {
        return bid;
    }

    public String getBamt() {
        return bamt;
    }

    public String getBpaid() {
        return bpaid;
    }

    public String getBpayable() {
        return bpayable;
    }

    public String getBbalance() {
        return bbalance;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public void setBamt(String bamt) {
        this.bamt = bamt;
    }

    public void setBpaid(String bpaid) {
        this.bpaid = bpaid;
    }

    public void setBpayable(String bpayable) {
        this.bpayable = bpayable;
    }

    public void setBbalance(String bbalance) {
        this.bbalance = bbalance;
    }
}
