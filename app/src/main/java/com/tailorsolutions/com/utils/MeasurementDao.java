package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface MeasurementDao {

    @Insert
    void insert(Measurement measurement);

    @Update
    void update(Measurement measurement);

    @Delete
    void delete(Measurement measurement);

    @Query("DELETE FROM tbl_measurement")
    void deleteAll();

    @Query("SELECT * FROM tbl_measurement")
    LiveData<List<Measurement>> getAll();

    @Query("SELECT * FROM tbl_measurement where mid = :mid")
    LiveData<List<Measurement>> getAll(int mid);

    @Query("SELECT * FROM tbl_measurement WHERE cid = :cid and mtype=:mtype")
    LiveData<List<Measurement>> getAll(String cid,String mtype);
}
