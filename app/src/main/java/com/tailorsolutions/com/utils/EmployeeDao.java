package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface EmployeeDao {
    @Insert
    void insert(Salesman employee);

    @Update
    void update(Salesman employee);

    @Delete
    void delete(Salesman employye);

    @Query("DELETE FROM tbl_salesman")
    void deleteAllEmployee();

    @Query("SELECT * FROM tbl_salesman ORDER BY sid DESC")
    LiveData<List<Salesman>> getAllEmployee();

    @Query("SELECT * FROM tbl_salesman WHERE stype='salesman'")
    LiveData<List<Salesman>> getAllSalesman();

    @Query("SELECT * FROM tbl_salesman WHERE stype='tailor'")
    LiveData<List<Salesman>> getAllTailor();
}
