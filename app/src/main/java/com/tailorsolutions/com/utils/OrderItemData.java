package com.tailorsolutions.com.utils;

import java.util.ArrayList;
import java.util.List;

public class OrderItemData {
    ItemMaster item;
    public String qty;
    public static String cid;
    public boolean checkstatus=false;
    public static String ostate;
    public static String t3ms="";
    public static String t2ms="";
    public static String b3ms="";
    public static String b2ms="";
    public ItemMaster getItem() {
        return item;
    }

    public void setItem(ItemMaster item) {
        this.item = item;
    }
    public static List<ItemMaster> udata = new ArrayList<>();
    public static List<ItemMaster> ldata = new ArrayList<>();
    public static List<OrderItemData> data = new ArrayList<>();
    public static List<ItemMaster> itemdata = new ArrayList<>();
    public static List<String> common = new ArrayList<>();
    public static List<String> ucommon = new ArrayList<>();
    public static List<Measurement> umeasurementsList = new ArrayList<>();
    public static List<Measurement> lmeasurementsList = new ArrayList<>();
    public static List<Measurement> tmeasurementsList = new ArrayList<>();

}
