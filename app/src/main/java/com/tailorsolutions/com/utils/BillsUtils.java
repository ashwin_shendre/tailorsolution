package com.tailorsolutions.com.utils;

import java.util.ArrayList;
import java.util.List;

public class BillsUtils {
    private Bill bill;
    private String cname;
    public static String billid,billamt,billpaid,billpayable;
    public Bill getBill() {
        return bill;
    }

    public String getCname() {
        return cname;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public static List<BillsUtils> billsUtils = new ArrayList<>();

    public static List<Measurement> umeasurements = new ArrayList<>();
    public static List<Measurement> lmeasurements = new ArrayList<>();
    public static List<Measurement> tmeasurements = new ArrayList<>();
    public static List<Orders> orderslist = new ArrayList<>();
    public static List<Orders> lorders = new ArrayList<>();
    public static List<Orders> uorders = new ArrayList<>();
}
