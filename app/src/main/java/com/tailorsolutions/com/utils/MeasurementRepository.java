package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class MeasurementRepository {
    MeasurementDao measurementDao;
    LiveData<List<Measurement>> measurement;

    public MeasurementRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);
        measurementDao = database.measurementDao();
        measurement = measurementDao.getAll();
    }

    public void insert(Measurement measurement){
        new MeasurementRepository.InsertAsync(measurementDao).execute(measurement);
    }
    public void update(Measurement measurement){
        new MeasurementRepository.UpdateAsync(measurementDao).execute(measurement);
    }
    public void delete(Measurement measurement){
        new MeasurementRepository.DeleteAsync(measurementDao).execute(measurement);
    }

    public void deleteAll(){
        new MeasurementRepository.DeleteAllAsync(measurementDao).execute();
    }
    public LiveData<List<Measurement>> getAll(){
        return  measurement;
    }
    public LiveData<List<Measurement>> getAllUpper(String cid){
        return  measurementDao.getAll(cid,"upper");
    }
    public LiveData<List<Measurement>> getAlllower(String cid){
        return  measurementDao.getAll(cid,"lower");
    }

    public LiveData<List<Measurement>> getAll(int mid){
        return measurementDao.getAll(mid);
    }

    private static class InsertAsync extends AsyncTask<Measurement,Void,Void> {

        private MeasurementDao measurementDao;
        private InsertAsync(MeasurementDao measurementDao){
            this.measurementDao = measurementDao;
        }


        @Override
        protected Void doInBackground(Measurement... measurements) {
            measurementDao.insert(measurements[0]);
            return null;
        }
    }
    private static class UpdateAsync extends AsyncTask<Measurement,Void,Void>{

        private MeasurementDao measurementDao;
        private UpdateAsync(MeasurementDao measurementDao){
            this.measurementDao = measurementDao;
        }


        @Override
        protected Void doInBackground(Measurement... measurements) {
            measurementDao.update(measurements[0]);
            return null;
        }
    }
    private static class DeleteAsync extends AsyncTask<Measurement,Void,Void>{

        private MeasurementDao measurementDao;
        private DeleteAsync(MeasurementDao measurementDao){
            this.measurementDao = measurementDao;
        }


        @Override
        protected Void doInBackground(Measurement... measurements) {
            measurementDao.delete(measurements[0]);
            return null;
        }
    }

    private static class DeleteAllAsync extends AsyncTask<Void,Void,Void>{

        private MeasurementDao measurementDao;
        private DeleteAllAsync(MeasurementDao measurementDao){
            this.measurementDao = measurementDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            measurementDao.deleteAll();
            return null;
        }
    }
}
