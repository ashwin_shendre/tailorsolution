package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class BillViewModel extends AndroidViewModel {
    BillRepository repository;
    LiveData<List<Bill>> bill;
    public BillViewModel(@NonNull Application application) {
        super(application);
        repository = new BillRepository(application);
        bill = repository.getAll();
    }

    public void insert(Bill bill){repository.insert(bill);}
    public void update(Bill bill){repository.update(bill);}
    public void delete(Bill bill){repository.delete(bill);}
    public void deleteAll(){repository.deleteAll();}
    public LiveData<List<Bill>> getAll(){return bill;}
    public LiveData<List<Bill>> getBill(String cid){return repository.getBill(cid);}
}
