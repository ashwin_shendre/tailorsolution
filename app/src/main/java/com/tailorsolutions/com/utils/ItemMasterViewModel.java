package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class ItemMasterViewModel extends AndroidViewModel {

    ItemMasterRepository repository;
    LiveData<List<ItemMaster>> item;

    public ItemMasterViewModel(@NonNull Application application) {
        super(application);
        repository = new ItemMasterRepository(application);
        item = repository.getAll();
    }
    public void insert(ItemMaster itemMaster){
        repository.insert(itemMaster);
    }
    public  void update(ItemMaster itemMaster){
        repository.update(itemMaster);
    }
    public void delete(ItemMaster itemMaster){
        repository.delete(itemMaster);
    }
    public void deleteAll(){
        repository.deleteAll();
    }
    public LiveData<List<ItemMaster>> getAll(){
        return repository.getAll();
    }
}
