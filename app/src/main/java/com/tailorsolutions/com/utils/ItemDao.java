package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ItemDao {

    @Insert
    void insert(ItemMaster item);

    @Update
    void update(ItemMaster item);

    @Delete
    void delete(ItemMaster item);

    @Query("DELETE FROM tbl_item")
    void deleteAll();

    @Query("SELECT * FROM tbl_item")
    LiveData<List<ItemMaster>> getAll();

}
