package com.tailorsolutions.com.utils;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {Salesman.class,Customer.class,ItemMaster.class,Orders.class,Measurement.class,Bill.class},version = 2)
public abstract class TailorDatabase extends RoomDatabase {
    private static TailorDatabase instance;

    public abstract EmployeeDao employeeDao();
    public abstract CustomerDao customerDao();
    public abstract ItemDao itemDao();
    public abstract OrderDao orderDao();
    public abstract MeasurementDao measurementDao();
    public abstract BillDao billDao();

    public static synchronized TailorDatabase getInstance(Context context){
        if(instance==null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    TailorDatabase.class,"DbTailor")
                    .addCallback(roomcallback)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomcallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateAsync(instance).execute();
        }
    };
    private static class PopulateAsync extends AsyncTask<Void,Void,Void>{
        ItemDao itemDao;

        private PopulateAsync(TailorDatabase employeeDao){
            this.itemDao = employeeDao.itemDao();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            itemDao.insert(new ItemMaster("Kurta","upper","Stand collor,Kurta patti,cuff,no cuff,whatsapp"));
            itemDao.insert(new ItemMaster("Payjama","lower",""));
            itemDao.insert(new ItemMaster("Jacket","upper","Plain,Whatsapp"));
            itemDao.insert(new ItemMaster("Pant","lower",""));
            itemDao.insert(new ItemMaster("Sherwani","upper",""));
            itemDao.insert(new ItemMaster("Jodhpuri","upper","Basic,Pattern"));
            itemDao.insert(new ItemMaster("Chudidar","lower",""));
            itemDao.insert(new ItemMaster("Feta","lower",""));
            itemDao.insert(new ItemMaster("Mozadi","lower",""));
            itemDao.insert(new ItemMaster("Tilak","accessories",""));
            itemDao.insert(new ItemMaster("Feather","accessories",""));
            itemDao.insert(new ItemMaster("Odhni","accessories",""));
            itemDao.insert(new ItemMaster("Indo","upper","collor cuff button,One side,Half work,Up down work,trade work,All over,Maharaja"));
            itemDao.insert(new ItemMaster("Peshawar","upper",""));
            itemDao.insert(new ItemMaster("Balloon Pant","lower",""));
            itemDao.insert(new ItemMaster("Nawabi Pant","upper",""));
            itemDao.insert(new ItemMaster("Hancky Pant","accessories",""));
            itemDao.insert(new ItemMaster("Brouch","accessories",""));
            itemDao.insert(new ItemMaster("Tuxido","upper",""));
            itemDao.insert(new ItemMaster("Waist Coat","upper",""));
            itemDao.insert(new ItemMaster("Shirt","upper","USA  cut,Plain,Tux"));
            itemDao.insert(new ItemMaster("Tie","accessories",""));
            itemDao.insert(new ItemMaster("Bow","accessories",""));
            itemDao.insert(new ItemMaster("Dhoti","lower",""));
            itemDao.insert(new ItemMaster("Haldi Dress","upper",""));
            itemDao.insert(new ItemMaster("Panch","accessories",""));
            itemDao.insert(new ItemMaster("Basic S.B 2 Pc","two piece",""));
            itemDao.insert(new ItemMaster("Basic S.B 3 Pc","three piece",""));
            itemDao.insert(new ItemMaster("Tuxido 3.Pc","three piece",""));
            itemDao.insert(new ItemMaster("Tuxido 2.Pc","two piece",""));
            return null;
        }
    }
}
