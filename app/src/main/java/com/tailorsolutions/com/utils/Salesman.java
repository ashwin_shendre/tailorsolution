package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_salesman")
public class Salesman {
    @PrimaryKey(autoGenerate = true)
    private int sid;
    private String sname;
    private String address;
    private String mobile;
    private String birthdate;
    private String email;
    private String status;
    private String stype;

    public Salesman(String sname,String address,String mobile,String birthdate,String email, String status,String stype){
        this.sname = sname;
        this.address = address;
        this.mobile = mobile;
        this.birthdate = birthdate;
        this.email = email;
        this.status = status;
        this.stype = stype;
    }

    public int getSid() {
        return sid;
    }

    public String getSname() {
        return sname;
    }

    public String getAddress() {
        return address;
    }

    public String getMobile() {
        return mobile;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getStype() {
        return stype;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }
}
