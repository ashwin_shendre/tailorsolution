package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Update;
import android.os.AsyncTask;

import java.util.List;

public class EmployeeRepository {
    private EmployeeDao employeeDao;
    private LiveData<List<Salesman>> allEmployee;

    public EmployeeRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);

        employeeDao = database.employeeDao();
        allEmployee = employeeDao.getAllEmployee();
    }

    public void insert(Salesman salesman){
        new InsertAsyncTask(employeeDao).execute(salesman);
    }
    public void update(Salesman salesman){
        new UpdateAsyncTask(employeeDao).execute(salesman);
    }
    public void delete(Salesman salesman){
        new DeleteAsyncTask(employeeDao).execute(salesman);
    }
    public void deleteAll(){
        new DeleteAllAsyncTask(employeeDao).execute();
    }
    public LiveData<List<Salesman>> getAllEmployee(){
        return allEmployee;
    }
    public LiveData<List<Salesman>> getAllSalesman(){
        return employeeDao.getAllSalesman();
    }

    public LiveData<List<Salesman>> getAllTailor(){
        return employeeDao.getAllTailor();
    }

    private static class InsertAsyncTask extends AsyncTask<Salesman,Void,Void>{
        private EmployeeDao employeeDao;

        private InsertAsyncTask(EmployeeDao employeeDao){
            this.employeeDao = employeeDao;
        }
        @Override
        protected Void doInBackground(Salesman... salesmen) {
            employeeDao.insert(salesmen[0]);
            return null;
        }
    }
    private static class UpdateAsyncTask extends AsyncTask<Salesman,Void,Void>{
        private EmployeeDao employeeDao;

        private UpdateAsyncTask(EmployeeDao employeeDao){
            this.employeeDao = employeeDao;
        }
        @Override
        protected Void doInBackground(Salesman... salesmen) {
            employeeDao.update(salesmen[0]);
            return null;
        }
    }
    private static class DeleteAsyncTask extends AsyncTask<Salesman,Void,Void>{
        private EmployeeDao employeeDao;

        private DeleteAsyncTask(EmployeeDao employeeDao){
            this.employeeDao = employeeDao;
        }
        @Override
        protected Void doInBackground(Salesman... salesmen) {
            employeeDao.delete(salesmen[0]);
            return null;
        }
    }
    private static class DeleteAllAsyncTask extends AsyncTask<Salesman,Void,Void>{
        private EmployeeDao employeeDao;

        private DeleteAllAsyncTask(EmployeeDao employeeDao){
            this.employeeDao = employeeDao;
        }
        @Override
        protected Void doInBackground(Salesman... salesmen) {
            employeeDao.deleteAllEmployee();
            return null;
        }
    }

}
