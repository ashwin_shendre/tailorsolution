package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class MeasurementViewModel extends AndroidViewModel {
    public MeasurementRepository repository;
    public LiveData<List<Measurement>> measurement;
    public MeasurementViewModel(@NonNull Application application) {
        super(application);
        repository = new MeasurementRepository(application);
        measurement = repository.getAll();
    }

    public void insert(Measurement measurements){repository.insert(measurements);}

    public void update(Measurement measurements){repository.update(measurements);}
    public void delete(Measurement measurements){repository.delete(measurements);}
    public void deleteAll(){repository.deleteAll();}
    public LiveData<List<Measurement>> getAll(int mid){return repository.getAll(mid);}
    public LiveData<List<Measurement>> getAll(){return measurement;}
    public LiveData<List<Measurement>> getAllUpper(String cid){return repository.getAllUpper(cid);}
    public LiveData<List<Measurement>> getAllLower(String cid){return repository.getAlllower(cid);}
}
