package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class EmployeeViewModel extends AndroidViewModel {
    private EmployeeRepository employeeRepository;
    LiveData<List<Salesman>> employee;
    public EmployeeViewModel(@NonNull Application application) {
        super(application);
        employeeRepository = new EmployeeRepository(application);
        employee = employeeRepository.getAllEmployee();
    }

    public void insert(Salesman employee){
        employeeRepository.insert(employee);
    }
    public void update(Salesman employee){
        employeeRepository.update(employee);
    }
    public void delete(Salesman employee){
        employeeRepository.delete(employee);
    }
    public void deleteAll(){
        employeeRepository.deleteAll();
    }
    public LiveData<List<Salesman>> getEmployee(){
        return employee;
    }

    public LiveData<List<Salesman>> getSalesman(){
        return employeeRepository.getAllSalesman();
    }

    public LiveData<List<Salesman>> getTailor(){
        return employeeRepository.getAllTailor();
    }
}
