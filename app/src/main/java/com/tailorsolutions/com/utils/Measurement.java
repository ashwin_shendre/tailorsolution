package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_measurement")
public class Measurement {

    @PrimaryKey(autoGenerate = true)
    private int mid;
    private String length;
    private String waist;
    private String flip;
    private String thie;
    private String knee;
    private String knee2;
    private String bottom;
    private String folk;
    private String chest;
    private String stomach;
    private String hip;
    private String shoulder;
    private String sleeve;
    private String arm;
    private String neck;
    private String comments;
    private String photo;
    private String status;
    private String ddate;
    private String adate;
    private String sid;
    private String cid;
    private String mtype;
    private String specification;
    private String cmts;

    public Measurement(String length,String waist,String flip,String thie,String knee,String knee2,String bottom,String folk,String chest,String hip,String stomach,String shoulder,String sleeve,String arm,String neck,String comments,String photo,String status,String ddate,String adate,String sid,String cid,String mtype,String specification,String cmts){
        this.length = length;
        this.waist = waist;
        this.flip=flip;
        this.thie = thie;
        this.knee = knee;
        this.knee2 = knee2;
        this.bottom = bottom;
        this.folk = folk;
        this.chest = chest;
        this.stomach = stomach;
        this.hip = hip;
        this.shoulder = shoulder;
        this.sleeve = sleeve;
        this.arm = arm;
        this.neck = neck;
        this. comments = comments;
        this.photo = photo;
        this.status = status;
        this.ddate = ddate;
        this.adate = adate;
        this.sid = sid;
        this.cid = cid;
        this.mtype=mtype;
        this.specification = specification;
        this.cmts = cmts;
    }

    public String getSpecification() {
        return specification;
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public String getCid() {
        return cid;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setWaist(String waist) {
        this.waist = waist;
    }

    public void setFlip(String flip) {
        this.flip = flip;
    }

    public void setThie(String thie) {
        this.thie = thie;
    }

    public void setKnee(String knee) {
        this.knee = knee;
    }

    public void setKnee2(String knee2) {
        this.knee2 = knee2;
    }

    public void setBottom(String bottom) {
        this.bottom = bottom;
    }

    public void setFolk(String folk) {
        this.folk = folk;
    }

    public void setChest(String chest) {
        this.chest = chest;
    }

    public void setStomach(String stomach) {
        this.stomach = stomach;
    }

    public void setShoulder(String shoulder) {
        this.shoulder = shoulder;
    }

    public void setSleeve(String sleeve) {
        this.sleeve = sleeve;
    }

    public void setArm(String arm) {
        this.arm = arm;
    }

    public void setNeck(String neck) {
        this.neck = neck;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDdate(String ddate) {
        this.ddate = ddate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }


    public String getHip() {
        return hip;
    }

    public void setHip(String hip) {
        this.hip = hip;
    }

    public int getMid() {
        return mid;
    }

    public String getLength() {
        return length;
    }

    public String getWaist() {
        return waist;
    }

    public String getFlip() {
        return flip;
    }

    public String getThie() {
        return thie;
    }

    public String getKnee() {
        return knee;
    }

    public String getKnee2() {
        return knee2;
    }

    public String getBottom() {
        return bottom;
    }

    public String getFolk() {
        return folk;
    }

    public String getChest() {
        return chest;
    }

    public String getStomach() {
        return stomach;
    }

    public String getShoulder() {
        return shoulder;
    }

    public String getSleeve() {
        return sleeve;
    }

    public String getArm() {
        return arm;
    }

    public String getNeck() {
        return neck;
    }

    public String getComments() {
        return comments;
    }

    public String getPhoto() {
        return photo;
    }

    public String getStatus() {
        return status;
    }

    public String getDdate() {
        return ddate;
    }

    public String getAdate() {
        return adate;
    }

    public String getSid() {
        return sid;
    }



}
