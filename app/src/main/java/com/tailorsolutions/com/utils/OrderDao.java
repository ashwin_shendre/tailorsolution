package com.tailorsolutions.com.utils;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface OrderDao {
    @Insert
    void insert(Orders orders);

    @Update
    void update(Orders orders);

    @Delete
    void delete(Orders orders);

    @Query("DELETE FROM tbl_orders")
    void deleteAll();

    @Query("SELECT * FROM tbl_orders")
    LiveData<List<Orders>> getAll();

    @Query("SELECT * FROM tbl_orders WHERE billid = :id")
    LiveData<List<Orders>> getCOrders(String id);
}
