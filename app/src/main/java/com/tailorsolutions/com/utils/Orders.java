package com.tailorsolutions.com.utils;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbl_orders")
public class Orders {
    @PrimaryKey(autoGenerate = true)
    private int oid;
    private String item_id;
    private String i_name;
    private String Qty;
    private String cid;
    private String measureid;
    private String billid;
    private String type;
    private String amt;

    public Orders(String item_id,String i_name,String Qty,String cid,String measureid,String billid,String type,String amt){
        this.item_id = item_id;
        this.i_name = i_name;
        this.Qty = Qty;
        this.cid = cid;
        this.measureid = measureid;
        this.billid = billid;
        this.type = type;
        this.amt = amt;
    }

    public String getI_name() {
        return i_name;
    }

    public void setI_name(String i_name) {
        this.i_name = i_name;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCid() {
        return cid;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public void setMeasureid(String measureid) {
        this.measureid = measureid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public int getOid() {
        return oid;
    }

    public String getItem_id() {
        return item_id;
    }

    public String getQty() {
        return Qty;
    }

    public String getMeasureid() {
        return measureid;
    }

    public String getBillid() {
        return billid;
    }

    public String getType() {
        return type;
    }

    public String getAmt() {
        return amt;
    }
}
