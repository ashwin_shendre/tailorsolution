package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class OrderRepository {
    OrderDao orderDao;
    LiveData<List<Orders>> orders;

    public OrderRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);
        orderDao = database.orderDao();
        orders = orderDao.getAll();
    }

    public void insert(Orders order){
        new OrderRepository.InsertAsync(orderDao).execute(order);
    }
    public void update(Orders order){
        new OrderRepository.UpdateAsync(orderDao).execute(order);
    }
    public void delete(Orders order){
        new OrderRepository.DeleteAsync(orderDao).execute(order);
    }

    public void deleteAll(){
        new OrderRepository.DeleteAllAsync(orderDao).execute();
    }
    public LiveData<List<Orders>> getAll(){
        return  orders;
    }
    public LiveData<List<Orders>> getAll(String id){

        //orders=(LiveData<List<Orders>>) new GetAllCustomerOrdersAsync(orderDao).execute(id);
        return orderDao.getCOrders(id);
    }

    private static class InsertAsync extends AsyncTask<Orders,Void,Void> {

        private OrderDao orderDao;
        private InsertAsync(OrderDao orderDao){
            this.orderDao = orderDao;
        }


        @Override
        protected Void doInBackground(Orders... orders) {
            orderDao.insert(orders[0]);
            return null;
        }
    }
    private static class UpdateAsync extends AsyncTask<Orders,Void,Void>{

        private OrderDao orderDao;
        private UpdateAsync(OrderDao orderDao){
            this.orderDao = orderDao;
        }


        @Override
        protected Void doInBackground(Orders... orders) {
            orderDao.update(orders[0]);
            return null;
        }
    }
    private static class DeleteAsync extends AsyncTask<Orders,Void,Void>{

        private OrderDao orderDao;
        private DeleteAsync(OrderDao orderDao){
            this.orderDao = orderDao;
        }


        @Override
        protected Void doInBackground(Orders... orders) {
            orderDao.delete(orders[0]);
            return null;
        }
    }

    private static class GetAllCustomerOrdersAsync extends AsyncTask<String,Void,LiveData<List<Orders>>>{

        private OrderDao orderDao;
        private GetAllCustomerOrdersAsync(OrderDao orderDao){
            this.orderDao = orderDao;
        }


        @Override
        protected LiveData<List<Orders>> doInBackground(String... strings) {

            return orderDao.getCOrders(strings[0]);

        }
    }

    private static class DeleteAllAsync extends AsyncTask<Void,Void,Void>{

        private OrderDao orderDao;
        private DeleteAllAsync(OrderDao orderDao){
            this.orderDao = orderDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            orderDao.deleteAll();
            return null;
        }
    }
}
