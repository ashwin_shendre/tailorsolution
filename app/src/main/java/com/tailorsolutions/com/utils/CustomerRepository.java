package com.tailorsolutions.com.utils;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class CustomerRepository {
    CustomerDao customerDao;
    LiveData<List<Customer>> customer;

    public CustomerRepository(Application application){
        TailorDatabase database = TailorDatabase.getInstance(application);
        customerDao = database.customerDao();
        customer = customerDao.getAll();
    }

    public void insert(Customer customer){
        new InsertAsync(customerDao).execute(customer);
    }
    public void update(Customer customer){
        new UpdateAsync(customerDao).execute(customer);
    }
    public void delete(Customer customer){
        new DeleteAsync(customerDao).execute(customer);
    }

    public void deleteAll(){
        new DeleteAllAsync(customerDao).execute();
    }
    public LiveData<List<Customer>> getAll(){
        return  customer;
    }
    public LiveData<List<Customer>> getCustomer(int cid){
        return  customerDao.getCustomer(cid);
    }

    private static class InsertAsync extends AsyncTask<Customer,Void,Void>{

        private CustomerDao customerDao;
        private InsertAsync(CustomerDao customerDao){
            this.customerDao=customerDao;
        }

        @Override
        protected Void doInBackground(Customer... customers) {
            customerDao.insert(customers[0]);
            return null;
        }
    }
    private static class UpdateAsync extends AsyncTask<Customer,Void,Void>{

        private CustomerDao customerDao;
        private UpdateAsync(CustomerDao customerDao){
            this.customerDao=customerDao;
        }

        @Override
        protected Void doInBackground(Customer... customers) {
            customerDao.update(customers[0]);
            return null;
        }
    }
    private static class DeleteAsync extends AsyncTask<Customer,Void,Void>{

        private CustomerDao customerDao;
        private DeleteAsync(CustomerDao customerDao){
            this.customerDao=customerDao;
        }

        @Override
        protected Void doInBackground(Customer... customers) {
            customerDao.delete(customers[0]);
            return null;
        }
    }

    private static class DeleteAllAsync extends AsyncTask<Void,Void,Void>{

        private CustomerDao customerDao;
        private DeleteAllAsync(CustomerDao customerDao){
            this.customerDao=customerDao;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            customerDao.deleteAll();
            return null;
        }
    }

}
