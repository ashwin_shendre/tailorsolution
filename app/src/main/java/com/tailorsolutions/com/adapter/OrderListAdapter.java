package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Orders;

import java.util.ArrayList;
import java.util.List;

public class OrderListAdapter extends BaseAdapter {
    List<Orders> orders = new ArrayList<>();
    Context context;
    public OrderListAdapter(Context context){
        this.context = context;
    }
    @Override
    public int getCount() {
        if (orders!=null)
            return orders.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_customer_layout,null);
        TextView txt = (TextView) convertView.findViewById(R.id.txt_mob);
        //getItemName(orders.get(position).getItem_id());
        txt.setText(orders.get(position).getI_name());
        return convertView;
    }



    public void setOrders(List<Orders> orders){
        this.orders = orders;
        notifyDataSetChanged();
    }
}
