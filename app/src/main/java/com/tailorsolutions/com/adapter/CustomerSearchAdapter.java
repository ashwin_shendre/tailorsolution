package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerSearchAdapter extends ArrayAdapter<Customer> {

    Context context;
    int resource, textViewResourceId;
    List<Customer> items, tempItems, suggestions;

    public CustomerSearchAdapter( Context context, int resource,int textViewResourceId,List<Customer> items) {
        super(context, resource,textViewResourceId,items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        this.tempItems = new ArrayList<Customer>(items);
        suggestions = new ArrayList<Customer>();

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_customer_layout,parent,false);
        TextView txtmob = (TextView) convertView.findViewById(R.id.txt_mob);
        txtmob.setText(items.get(position).getCmobile());
        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((Customer) resultValue).getCmobile();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Customer people : tempItems) {
                    if (people.getCmobile().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Customer> filterList = (ArrayList<Customer>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Customer people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
