package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.tailoringsolutions.ViewOrdersActivity;
import com.tailorsolutions.com.utils.Bill;
import com.tailorsolutions.com.utils.BillsUtils;
import com.tailorsolutions.com.utils.Orders;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.CheckedOutputStream;

public class BillAdapter extends BaseAdapter {
    List<BillsUtils> bills = new ArrayList<>();
    Context context;
    public BillAdapter(Context context){
        this.context = context;
    }
    @Override
    public int getCount() {
        if (bills.size()>0)
            return bills.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_customer_layout,null);
        TextView txt = (TextView) convertView.findViewById(R.id.txt_mob);
        TextView txtc = (TextView) convertView.findViewById(R.id.txt_cnm);
        //getItemName(orders.get(position).getItem_id());
        txt.setText("Bill :"+bills.get(position).getBill().getBid());
        txtc.setText("Customer :"+bills.get(position).getCname());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewIntent = new Intent(context, ViewOrdersActivity.class);
                BillsUtils.billid = ""+bills.get(position).getBill().getBid();
                BillsUtils.billamt = bills.get(position).getBill().getBamt();
                BillsUtils.billpaid = bills.get(position).getBill().getBpaid();
                BillsUtils.billpayable = bills.get(position).getBill().getBpayable();
                viewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(viewIntent);
            }
        });
        return convertView;
    }
    public void setOrders(List<BillsUtils> bills){
        this.bills = bills;
        notifyDataSetChanged();
    }
}
