package com.tailorsolutions.com.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tailorsolutions.com.fragments.FragmentLower;
import com.tailorsolutions.com.fragments.FragmentUpper;
import com.tailorsolutions.com.fragments.OrderPageFragment;
import com.tailorsolutions.com.fragments.ViewFragmentLower;
import com.tailorsolutions.com.fragments.ViewFragmentUpper;
import com.tailorsolutions.com.fragments.ViewOrderFragment;
import com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity;
import com.tailorsolutions.com.tailoringsolutions.ViewOrdersActivity;

public class ViewBillPagerAdapter extends FragmentPagerAdapter {
    public ViewBillPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:return new ViewOrderFragment();
            case 1:return new ViewFragmentUpper();
            case 2:return new ViewFragmentLower();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return ViewOrdersActivity.title[position];
    }
}
