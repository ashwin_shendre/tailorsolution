package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.tailorsolutions.com.activity.SalesPersonActivity;
import com.tailorsolutions.com.fragments.FragmentLower;
import com.tailorsolutions.com.fragments.FragmentUpper;
import com.tailorsolutions.com.fragments.LowerMeasurementFragment;
import com.tailorsolutions.com.fragments.UpperMeasurementFragment;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.OrderItemData;
import com.tailorsolutions.com.utils.Salesman;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends BaseAdapter {
    Context context;
    List<OrderItemData> emp = new ArrayList<>();
    FragmentManager fm;
    EditText edt_qty;
    public EmployeeAdapter(Context context,FragmentManager fm){
        this.context = context;
        this.fm = fm;
    }

    @Override
    public int getCount() {
        if (emp.size()>0)
            return emp.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return OrderItemData.data.get(position);
    }

    @Override
    public int getViewTypeCount() {
        if (emp.size()>0)
            return emp.size();
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_order,null);
        OrderItemData obj = getDetails(position);
        TextView txt_nm=(TextView) convertView.findViewById(R.id.tviname);
        TextView txt_msr = (TextView) convertView.findViewById(R.id.tvmeasure);
        edt_qty = (EditText) convertView.findViewById(R.id.edt_qyt);
        edt_qty.setText(emp.get(position).qty);
        CheckBox cb = (CheckBox) convertView.findViewById(R.id.cb_item);
        cb.setTag(position);
        cb.setChecked(obj.checkstatus);
        cb.setOnCheckedChangeListener(myCheckChangeList);
        txt_nm.setText(emp.get(position).getItem().getIname());
        txt_msr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteData(emp.get(position));

                emp.remove(position);
                notifyDataSetChanged();
            }
        });
        edt_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                OrderItemData.data.get(position).qty = edt_qty.getText().toString();
            }
        });
        return convertView;
    }
    public OrderItemData getDetails(int position){
        return ((OrderItemData)getItem(position));
    }
    public void setData(List<OrderItemData> data){
        emp = data;
        notifyDataSetChanged();
    }
    public ArrayList<OrderItemData> getBox(){
        //String attendance="";
        ArrayList<OrderItemData> box=new ArrayList<OrderItemData>();
        for (OrderItemData c:emp){
            if (c.checkstatus) {
                c.qty=edt_qty.getText().toString();
                box.add(c);
                //select_list.add(c.getPhone());
                //attendance = attendance+c.getItem().getId();
            }

        }
        return (box);
    }
    CompoundButton.OnCheckedChangeListener myCheckChangeList=new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            OrderItemData dt = getDetails((Integer) buttonView.getTag());
           getDetails((Integer) buttonView.getTag()).checkstatus=isChecked;
           if (isChecked)
               deleteData(dt);
           else
               addData(dt);

        }
    };

    private void addData(OrderItemData dt) {
        if (dt.getItem().getItype().equalsIgnoreCase("upper")){
            OrderItemData.udata.add(dt.getItem());
            FragmentUpper.setData();
        }
        else if (dt.getItem().getItype().equalsIgnoreCase("lower")){
            OrderItemData.ldata.add(dt.getItem());
        }

    }

    void deleteData(OrderItemData dt){
        if (dt.getItem().getItype().equalsIgnoreCase("upper")){
            for (int i=0;i<OrderItemData.udata.size();i++){
                if (OrderItemData.udata.get(i).getIname().equalsIgnoreCase(dt.getItem().getIname())) {
                    OrderItemData.udata.remove(i);

                    break;
                }
            }
            FragmentUpper.setData();
        }
        else if (dt.getItem().getItype().equalsIgnoreCase("lower")){
            for (int i=0;i<OrderItemData.ldata.size();i++){
                if (OrderItemData.ldata.get(i).getIname().equalsIgnoreCase(dt.getItem().getIname())) {
                    OrderItemData.ldata.remove(i);
                    break;
                }
            }
        }
        else{
            for (int i=0;i<OrderItemData.udata.size();i++){
                if (OrderItemData.udata.get(i).getIname().equalsIgnoreCase(dt.getItem().getIname())) {
                    OrderItemData.udata.remove(i);
                    FragmentUpper.flg=1;
                    FragmentLower.flg=1;
                    break;
                }
            }
            for (int i=0;i<OrderItemData.ldata.size();i++){
                if (OrderItemData.ldata.get(i).getIname().equalsIgnoreCase(dt.getItem().getIname())) {
                    OrderItemData.ldata.remove(i);
                    break;
                }
            }
            FragmentUpper.setData();
        }
    }
}
