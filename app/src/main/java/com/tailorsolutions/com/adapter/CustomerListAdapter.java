package com.tailorsolutions.com.adapter;

import android.content.ContentUris;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.activity.OrderPageActivity;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.List;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerHolder> {

    List<Customer> data;
    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        View cust = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.employee_item,viewGroup,false);
        cust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(viewGroup.getContext(),"Clicked="+data.get(i).getCid(),Toast.LENGTH_LONG).show();
                OrderItemData.cid = ""+data.get(i).getCid();
                Intent orderIntent = new Intent(viewGroup.getContext(), OrderPageActivity.class);
                orderIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                viewGroup.getContext().startActivity(orderIntent);
            }
        });
        CustomerHolder holder = new CustomerHolder(cust);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerHolder customerHolder, int i) {
        customerHolder.txt_nm.setText(data.get(i).getCname());
        customerHolder.txt_mob.setText(data.get(i).getCmobile());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setCust(List<Customer> data){
        this.data = data;
        notifyDataSetChanged();
    }

    class CustomerHolder extends RecyclerView.ViewHolder{
        TextView txt_nm,txt_mob;
        public CustomerHolder(@NonNull View itemView) {
            super(itemView);
            txt_nm = itemView.findViewById(R.id.tvName);
            txt_mob = itemView.findViewById(R.id.tvmobile);
        }
    }
}
