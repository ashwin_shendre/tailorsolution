package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.support.annotation.NonNull;
import android.widget.Filter;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.ItemMaster;

import java.util.ArrayList;
import java.util.List;

public class ItemAddAdapter extends ArrayAdapter<ItemMaster> {
    Context context;
    int resource, textViewResourceId;
    List<ItemMaster> items, tempItems, suggestions;



    public ItemAddAdapter(Context context, int resource, int textViewResourceId, List<ItemMaster> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = objects;
        this.tempItems = new ArrayList<ItemMaster>(objects);
        suggestions = new ArrayList<ItemMaster>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_customer_layout,parent,false);
        TextView txtmob = (TextView) convertView.findViewById(R.id.txt_mob);
        txtmob.setText(items.get(position).getIname());

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((ItemMaster) resultValue).getIname();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (ItemMaster people : tempItems) {
                    if (people.getIname().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<ItemMaster> filterList = (ArrayList<ItemMaster>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (ItemMaster people : filterList) {
                    add(people);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
