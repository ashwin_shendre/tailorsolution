package com.tailorsolutions.com.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tailorsolutions.com.fragments.FragmentLower;
import com.tailorsolutions.com.fragments.FragmentUpper;
import com.tailorsolutions.com.fragments.OrderPageFragment;
import com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity;

public class BillPagerAdapter extends FragmentPagerAdapter {
    public BillPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:return new OrderPageFragment();
            case 1:return new FragmentUpper();
            case 2:return new FragmentLower();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return NewOrderPageActivity.title[position];
    }
}
