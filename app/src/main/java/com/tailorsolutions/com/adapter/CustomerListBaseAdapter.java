package com.tailorsolutions.com.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.tailoringsolutions.BillActivity;
import com.tailorsolutions.com.tailoringsolutions.MeasurementActivity;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Customer;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.ArrayList;
import java.util.List;

public class CustomerListBaseAdapter extends BaseAdapter {
    Context context;
    List<Customer> data = new ArrayList<>();
    public CustomerListBaseAdapter(Context context){
        this.context = context;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.adapter_customer_layout,null);
        TextView txt_nm = (TextView) convertView.findViewById(R.id.txt_nm);
        txt_nm.setText(data.get(position).getCname());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderItemData.cid = ""+data.get(position).getCid();
                if (OrderItemData.ostate.equalsIgnoreCase("measurement")) {
                    Intent measurementIntent = new Intent(context, MeasurementActivity.class);
                    measurementIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(measurementIntent);
                }
                else if (OrderItemData.ostate.equalsIgnoreCase("bill")){
                    Intent measurementIntent = new Intent(context, BillActivity.class);
                    measurementIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(measurementIntent);
                }
            }
        });
        return convertView;
    }
    public void setData(List<Customer> data){
        this.data = data;
        notifyDataSetChanged();
    }
}
