package com.tailorsolutions.com.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.ItemMaster;

import java.util.ArrayList;
import java.util.List;

public class ItemMasterAdapter extends RecyclerView.Adapter<ItemMasterAdapter.ItemMasterHolder> {
    List<ItemMaster> data = new ArrayList<>();
    @NonNull
    @Override
    public ItemMasterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View vp = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.employee_item,viewGroup,false);
        return new ItemMasterHolder(vp);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemMasterHolder itemMasterHolder, int i) {
        itemMasterHolder.txt_nm.setText(data.get(i).getIname());
        itemMasterHolder.txt_mob.setText(data.get(i).getItype());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<ItemMaster> data){
        this.data = data;
        notifyDataSetChanged();
    }

    class ItemMasterHolder extends RecyclerView.ViewHolder {
        TextView txt_nm,txt_mob;
        public ItemMasterHolder(@NonNull View itemView) {
            super(itemView);
            txt_nm = itemView.findViewById(R.id.tvName);
            txt_mob = itemView.findViewById(R.id.tvmobile);
        }
    }
}
