package com.tailorsolutions.com.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Salesman;

import java.util.ArrayList;
import java.util.List;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.EmployeeHolder> {
    List<Salesman> data = new ArrayList<>();


    class EmployeeHolder extends RecyclerView.ViewHolder{
        private TextView txt_name,txt_mob;
        public EmployeeHolder(@NonNull View itemView) {
            super(itemView);
            txt_name = itemView.findViewById(R.id.tvName);
            txt_mob = itemView.findViewById(R.id.tvmobile);
        }
    }
    @NonNull
    @Override
    public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        //LayoutInflater inflater=
        View itemview = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.employee_item,viewGroup,false);

        return new EmployeeHolder(itemview);
    }

    @Override
    public void onBindViewHolder(EmployeeHolder viewHolder, int i) {
           viewHolder.txt_name.setText(data.get(i).getSname());
           viewHolder.txt_mob.setText(data.get(i).getMobile());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public void setEmp(List<Salesman> data){
        this.data = data;
        notifyDataSetChanged();
    }
}
