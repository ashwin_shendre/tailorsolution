package com.tailorsolutions.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.BillsUtils;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.ArrayList;
import java.util.List;

public class ViewFragmentUpper extends Fragment {
    View view;
    public static TableLayout tbl_layout;
    public static Context context;
    public static FragmentManager fm;
    public static List<Measurement> measures=new ArrayList<>();
    public static String[] measurements = {"Length", "Chest", "Stomach", "Hip", "Shoulder", "Sleeve", "Neck", "Arm"};
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_upper_measurement, container, false);
        TextView txtsave = (TextView) view.findViewById(R.id.txt_save);
        fm = getFragmentManager();
        tbl_layout = (TableLayout) view.findViewById(R.id.tbl_layout);
        txtsave.setVisibility(View.INVISIBLE);
        context = getActivity().getApplicationContext();
        return view;
    }
    public static void setData(){
        measures.clear();
        measures.addAll(BillsUtils.umeasurements);
        tbl_layout.removeAllViews();
        final TableRow tableRow = new TableRow(context);
        tableRow.setGravity(Gravity.CENTER);
        tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));
        //tableRow.setBackground(getResources().getDrawable(R.drawable.border));
        for (int i = 0; i < BillsUtils.uorders.size() + 1; i++) {
            // Add a TextView in the first column.
            if (i == 0) {
                TextView textView = new TextView(context);
                textView.setText("Measurs");
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                //textView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
                textView.setLayoutParams(new TableRow.LayoutParams(100, 50));
                textView.setTextSize(12f);
                tableRow.addView(textView, i);
            } else if (BillsUtils.uorders.get(i-1).getI_name().equalsIgnoreCase("Tuxido 3.Pc") || BillsUtils.uorders.get(i-1).getI_name().equalsIgnoreCase("Basic S.B 3 Pc")){
                TextView textView = new TextView(context);
                textView.setText("Blazer");
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                textView.setTextSize(12f);

                tableRow.addView(textView, i);
                i++;
                TextView textView1 = new TextView(context);
                textView1.setText("Waist coat");
                textView1.setPadding(5, 5, 5, 5);
                textView1.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView1.setBackground(context.getResources().getDrawable(R.drawable.border));
                textView1.setLayoutParams(new TableRow.LayoutParams(90, 50));
                textView1.setTextSize(12f);
                tableRow.addView(textView1, i);
            }else if(BillsUtils.uorders.get(i-1).getI_name().equalsIgnoreCase("Tuxido 2.Pc") || BillsUtils.uorders.get(i-1).getI_name().equalsIgnoreCase("Basic S.B 2 Pc")){
                TextView textView = new TextView(context);
                textView.setText("Blazer");
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                textView.setTextSize(12f);
                tableRow.addView(textView, i);
            }else {
                TextView textView = new TextView(context);
                textView.setText(BillsUtils.uorders.get(i - 1).getI_name());
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                textView.setTextSize(12f);
                final int ti = i;
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShowSpecificationDialog dialog = new ShowSpecificationDialog(measures.get(ti-1).getSpecification(),measures.get(ti-1).getCmts());
                        dialog.show(fm,"Show Specification");
                    }
                });
                tableRow.addView(textView, i);
            }
        }
        tbl_layout.addView(tableRow);
        for (int i = 1; i < measurements.length+1; i++) {
            TableRow tableRow1 = new TableRow(context);
            tableRow1.setGravity(Gravity.CENTER);
            tableRow1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));

            for (int j = 0; j < BillsUtils.uorders.size() + 1; j++) {
                // Add a TextView in the first column.
                if (j == 0) {
                    TextView textView = new TextView(context);
                    textView.setText(measurements[i-1]);
                    textView.setPadding(15, 5, 15, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    textView.setLayoutParams(new TableRow.LayoutParams(100, 50));
                    textView.setTextSize(12f);
                    tableRow1.addView(textView, j);
                } else {
                    TextView textView = new TextView(context);
                    //textView.setHint(args[j]);
                    textView.setPadding(5, 5, 5, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setHintTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    //SizeSearchAdapter adapter = new SizeSearchAdapter(context, R.layout.fragment_upper_measurement, R.id.txt_mob,data);
                    //textView.setAdapter(adapter);
                    switch (i){
                        case 1:textView.setText(measures.get(j-1).getLength());
                            break;
                        case 2:textView.setText(measures.get(j-1).getChest());
                            break;
                        case 3:textView.setText(measures.get(j-1).getStomach());
                            break;
                        case 4:textView.setText(measures.get(j-1).getHip());
                            break;
                        case 5:textView.setText(measures.get(j-1).getShoulder());
                            break;
                        case 6:textView.setText(measures.get(j-1).getSleeve());
                            break;
                        case 7:textView.setText(measures.get(j-1).getNeck());
                            break;
                        case 8:textView.setText(measures.get(j-1).getArm());
                            break;
                    }

                    textView.setInputType(InputType.TYPE_CLASS_NUMBER);
                    textView.setLayoutParams(new TableRow.LayoutParams(100, 50));
                    textView.setTextSize(12f);
                    tableRow1.addView(textView, j);
                }

            }
            tbl_layout.addView(tableRow1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }
}
