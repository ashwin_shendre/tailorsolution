package com.tailorsolutions.com.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.tailorsolutions.com.adapter.EmployeeAdapter;
import com.tailorsolutions.com.adapter.OrderListAdapter;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.tailoringsolutions.ViewOrdersActivity;
import com.tailorsolutions.com.utils.BillsUtils;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.ArrayList;
import java.util.List;

public class ViewOrderFragment extends Fragment {
    View view;
    public static OrderListAdapter adapter;
    EditText edt_paid,edt_total,edt_pending;
    ListView lstview;
    TextView txt_sv;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.vieworderlayout, container, false);
        lstview = (ListView) view.findViewById(R.id.lst_items);
        edt_paid = (EditText) view.findViewById(R.id.edt_paid);
        edt_total = (EditText) view.findViewById(R.id.edt_total);
        edt_pending = (EditText) view.findViewById(R.id.edt_payabel);
        txt_sv = (TextView) view.findViewById(R.id.btn_find);
        txt_sv.setVisibility(View.INVISIBLE);
        edt_total.setText(BillsUtils.billamt);
        edt_paid.setText(BillsUtils.billpaid);
        edt_pending.setText(BillsUtils.billpayable);
        adapter= new OrderListAdapter(getActivity().getApplicationContext());
        adapter.setOrders(ViewOrdersActivity.orders);
        lstview.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.setOrders(ViewOrdersActivity.orders);
        lstview.setAdapter(adapter);
    }
}
