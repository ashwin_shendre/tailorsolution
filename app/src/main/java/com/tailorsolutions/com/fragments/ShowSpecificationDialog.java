package com.tailorsolutions.com.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;

@SuppressLint("ValidFragment")
public class ShowSpecificationDialog extends DialogFragment {

    String spc,cmts;
    View view;
    TableLayout tbl_layout;
    EditText edt_cmt;
    @SuppressLint("ValidFragment")
    public ShowSpecificationDialog(String spc,String cmts){
        this.spc=spc;
        this.cmts = cmts;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_specification,container,false);
        TextView txt = (TextView) view.findViewById(R.id.btn_save);
        edt_cmt = (EditText) view.findViewById(R.id.edt_comments);
        //txt.setText(data);
        String[] tdt = spc.split(",");
        tbl_layout = (TableLayout) view.findViewById(R.id.tbl_layout);
        edt_cmt.setText(cmts);
        for (int i=0;i<tdt.length;i++){
            TableRow tableRow = new TableRow(getActivity().getApplicationContext());
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));
            TextView cb = new TextView(getActivity().getApplicationContext());
            cb.setText(tdt[i]);
            cb.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
            cb.setPadding(5,5,5,5);
            tableRow.addView(cb,0);
            tbl_layout.addView(tableRow);
        }
        txt.setVisibility(View.INVISIBLE);
        return view;
    }
}
