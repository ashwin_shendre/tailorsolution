package com.tailorsolutions.com.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.tailoringsolutions.R;

import org.w3c.dom.Text;

@SuppressLint("ValidFragment")
public class SpecificationDialog extends DialogFragment {

    View v;
    String data;
    TableLayout tbl_layout;
    int pos;
    String u;
    EditText edt_cmt;
    public SpecificationDialog(String data, int pos,String u){
        this.data = data;
        this.pos=pos;
        this.u = u;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_specification,container,false);
        TextView txt = (TextView) v.findViewById(R.id.btn_save);
        edt_cmt = (EditText) v.findViewById(R.id.edt_comments);
        //txt.setText(data);
        String[] tdt = data.split(",");
        tbl_layout = (TableLayout) v.findViewById(R.id.tbl_layout);
        if (data.length()>0) {
            for (int i = 0; i < tdt.length; i++) {
                TableRow tableRow = new TableRow(getActivity().getApplicationContext());
                tableRow.setGravity(Gravity.CENTER);
                tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));
                CheckBox cb = new CheckBox(getActivity().getApplicationContext());
                cb.setText(tdt[i]);
                cb.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                cb.setButtonDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back_checkbox));
                cb.setPadding(5, 5, 5, 5);
                tableRow.addView(cb, 0);
                tbl_layout.addView(tableRow);
            }
        }
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (u=="upper"){
                    FragmentUpper.cmts.set(pos,edt_cmt.getText().toString());
                    String str="";
                    for (int i=0;i<tbl_layout.getChildCount();i++){
                        TableRow  row = (TableRow) tbl_layout.getChildAt(i);
                        CheckBox cb = (CheckBox) row.getChildAt(0);

                        if (cb.isChecked()) {
                            str =str+","+ cb.getText();

                        }
                        FragmentUpper.spc.set(pos,str);

                        //Toast.makeText(getActivity().getApplicationContext(),str,Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    FragmentLower.cmts.set(pos,edt_cmt.getText().toString());
                    String str="";
                    for (int i=0;i<tbl_layout.getChildCount();i++){
                        TableRow  row = (TableRow) tbl_layout.getChildAt(i);
                        CheckBox cb = (CheckBox) row.getChildAt(0);

                        if (cb.isChecked()) {
                            str =str+","+ cb.getText();

                        }
                        FragmentLower.spc.set(pos,str);

                        //Toast.makeText(getActivity().getApplicationContext(),str,Toast.LENGTH_LONG).show();
                    }
                }
                dismiss();
            }
        });
        return v;
    }
}
