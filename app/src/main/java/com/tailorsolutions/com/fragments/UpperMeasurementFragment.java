package com.tailorsolutions.com.fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.ItemAddAdapter;
import com.tailorsolutions.com.common.SharedPreferenceDb;
import com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.ItemMaster;
import com.tailorsolutions.com.utils.ItemMasterViewModel;
import com.tailorsolutions.com.utils.OrderItemData;

import java.util.List;

@SuppressLint("ValidFragment")
public class UpperMeasurementFragment extends DialogFragment {

    View v;
    AutoCompleteTextView searchItem;
    List<ItemMaster> data;
    ItemAddAdapter adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_measurement,container,false);
        NewOrderPageActivity obj = new NewOrderPageActivity();
        /*NewOrderPageActivity.repository.getAll().observe(getActivity(), new Observer<List<ItemMaster>>() {
            @Override
            public void onChanged(@Nullable List<ItemMaster> itemMasters) {
                data = itemMasters;
            }
        });*/
        data = obj.getdata();
        searchItem = (AutoCompleteTextView) v.findViewById(R.id.itemsearch);
        adapter = new ItemAddAdapter(getActivity().getApplicationContext(),R.layout.fragment_measurement,R.id.txt_mob, data);
        searchItem.setAdapter(adapter);
        searchItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OrderItemData dt = new OrderItemData();
                ItemMaster master = (ItemMaster) parent.getItemAtPosition(position);
                dt.qty="1";
                dt.setItem(master);
                if (master.getItype().equalsIgnoreCase("upper")) {
                    OrderItemData.udata.add(master);
                    FragmentUpper.setData();
                }
                else if (master.getItype().equalsIgnoreCase("lower")) {
                    OrderItemData.ldata.add(master);
                    //FragmentLower.setData();
                }
                else {
                    OrderItemData.udata.add(master);
                    OrderItemData.ldata.add(master);
                    FragmentUpper.setData();
                }
                OrderItemData.data.add(dt);
                OrderPageFragment.adapter.setData(OrderItemData.data);
                OrderPageFragment.lst_orders.setAdapter(OrderPageFragment.adapter);
                dismiss();
            }
        });
        return v;
    }
}
