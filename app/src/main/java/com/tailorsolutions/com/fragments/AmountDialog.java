package com.tailorsolutions.com.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.tailorsolutions.com.tailoringsolutions.R;

public class AmountDialog extends DialogFragment {
    View view;
    EditText edt_paid,edt_total,edt_pending;
    TextView txt_save;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.amount_dialog,container,false);
        edt_paid = (EditText) view.findViewById(R.id.edt_paid);
        edt_total = (EditText) view.findViewById(R.id.edt_total);
        edt_pending = (EditText) view.findViewById(R.id.edt_payabel);
        txt_save  = (TextView) view.findViewById(R.id.btn_find);
        edt_paid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()>0) {
                    int total = Integer.parseInt(edt_total.getText().toString());
                    int paid = Integer.parseInt(s.toString());
                    edt_pending.setText("" + (total - paid));
                }
            }
        });
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderPageFragment.btotal = edt_total.getText().toString();
                OrderPageFragment.bpaid = edt_paid.getText().toString();
                OrderPageFragment.bpending = edt_pending.getText().toString();
                OrderPageFragment.stroeData();
                dismiss();
            }
        });
        return view;
    }
}
