package com.tailorsolutions.com.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.SizeSearchAdapter;
import com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.OrderItemData;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static com.tailorsolutions.com.utils.OrderItemData.common;

public class FragmentLower extends Fragment {


    View view;
    public static TableLayout tbl_layout;
    public static FragmentManager fm;
    public static List<String> spc=new ArrayList<>(),cmts=new ArrayList<>();
    public static int rs = 9, cls = 2,flg=1,flg1=1,iflg;
    //public static String[] args = {"Measures", "Common", "Pant", "Paijama", "Chudidar"};
    public static String[] measurements = {"Length", "Waist", "Hip", "Thie", "Knee 1", "Knee 2", "Bottom", "Folk", "Feta"};
    public static String[] measures = {"15","15 1/4", "15 1/2", "15 3/4", "16","16 1/4", "16 1/2", "16 3/4",
            "17","17 1/4", "17 1/2", "17 3/4","18", "18 1/4", "18 1/2", "18 3/4","19", "19 1/4", "19 1/2",
            "19 3/4","20", "20 1/4", "20 1/2", "20 3/4", "21 1/4","21", "21 1/2", "21 3/4", "22","22 1/4", "22 1/2",
            "22 3/4","23" ,"23 1/4", "23 1/2", "23 3/4","24", "24 1/4", "24 1/2", "24 3/4","25", "25 1/4",
            "25 1/2", "25 3/4","26", "26 1/4", "26 1/2", "26 3/4", "27","27 1/4", "27 1/2", "27 3/4","28", "28 1/4",
            "28 1/2", "28 3/4","29", "29 1/4", "29 1/2", "29 3/4","30", "30 1/4", "30 1/2","30 3/4","31", "31 1/4",
            "31 1/2", "31 3/4","33", "33 1/4", "33 1/2", "33 3/4","32","32 1/4", "32 1/2", "32 3/4","34", "34 1/4",
            "34 1/2", "34 3/4","35", "35 1/4","35 1/2", "35 3/4", "36","36 1/4", "36 1/2", "36 3/4","37", "37 1/4",
            "37 1/2", "38 3/4", "39","39 1/4", "39 1/2", "39 3/4", "40","40 1/4", "40 1/2", "40 3/4","41", "41 1/4",
            "41 1/2", "41 3/4","42", "42 1/4", "42 1/2", "42 3/4", "43","43 1/4", "43 1/2", "43 3/4","44", "44 1/4",
            "44 1/2", "44 3/4","45", "45 1/4", "45 1/2", "45 3/4", "46","46 1/4", "46 1/2", "46 3/4", "47","47 1/4",
            "47 1/2", "47 3/4","48", "48 1/4", "48 1/2", "48 3/4","49", "49 1/4", "49 1/2", "49 3/4","50", "50 1/4",
            "50 1/2", "50 3/4", "51","51 1/4", "51 1/2", "51 3/4", "52","52 1/4", "52 1/2", "52 3/4", "53","53 1/4",
            "53 1/2", "53 3/4", "54","54 1/4", "54 1/2", "54 3/4","55", "55 1/4", "55 1/2", "55 3/4","56", "56 1/4",
            "56 1/2", "56 3/4", "57","57 1/4", "57 1/2", "57 3/4","58", "58 1/4", "58 1/2", "58 3/4","59", "59 1/4",
            "59 1/2", "59 3/4", "60","60 1/4", "60 1/2", "60 3/4"};
    public static List<String> data = new ArrayList<>();
    public static Context context;
    TextView txt_save;

    @Nullable
    @Override
    public View onCreateView(@NonNull    LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frament_lower_measurement, container, false);
        tbl_layout = (TableLayout) view.findViewById(R.id.tbl_layout);
        txt_save = (TextView) view.findViewById(R.id.txt_save);
        context = getActivity().getApplicationContext();
        fm = getFragmentManager();
        data.clear();
        for (int i = 0; i < measures.length; i++) {
//>>>>>>> 46023ca4e6690cb55747f79c04f4bf8e8609d22f
            data.add(measures[i]);
        }
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indexdata = 1;

                List<String> measures = new ArrayList<>();
                int rowcount = tbl_layout.getChildCount();
                String dt = "";
                OrderItemData.lmeasurementsList.clear();
                OrderItemData.tmeasurementsList.clear();
                for (int j = 1; j < OrderItemData.ldata.size() + cls; j++) {
                    for (int i = 1; i < rowcount; i++) {
                        View row = tbl_layout.getChildAt(i);
                        TableRow trow = (TableRow) row;
                        int cnt = trow.getChildCount();
                        String len, chest, stomach, hip, shoulder, slen, neck, arm;
                        //for (int j = 1 ;j<cnt;j++){
                        View eview = trow.getChildAt(j);
                        EditText edview = (EditText) eview;
                        if (j == 1) {
                            common.add(edview.getText().toString());
                        } else {
                            if (edview.getText().toString().isEmpty()){
                                measures.add(common.get(i-1));
                            }
                            else
                                measures.add(edview.getText().toString());

                        }
                        //}
                        //indexdata++;
                    }
//<<<<<<< HEAD
                    if (j!=1) {
                        Measurement mes = new Measurement(measures.get(0), measures.get(1), "-", measures.get(3), measures.get(4), measures.get(5), measures.get(6), measures.get(7), "-", measures.get(2), "-", "-", "-", "-", "-", "some", "-", "Pending", "some", "some", "1", OrderItemData.cid, "lower",spc.get(j-2),cmts.get(j-2));
                        if (j==iflg){
                            OrderItemData.tmeasurementsList.add(mes);
                        }
                        OrderItemData.lmeasurementsList.add(mes);
                        measures.clear();
                    }
//=======
                    //             Measurement mes = new Measurement(measures.get(0), "-", "-", "-", "-", "-", "-", "-", measures.get(1), measures.get(2), measures.get(3), measures.get(4), measures.get(5), measures.get(6), measures.get(7), "some", "-", "Pending", "some", "some", "1", OrderItemData.cid, "lower");
//>>>>>>> 46023ca4e6690cb55747f79c04f4bf8e8609d22f
                    //measurementrepository.insert(mes);
                    //measurement_id++;
                    //orderrepository.insert(new Orders(""+d.getItem().getId(),d.getItem().getIname(),"2",OrderItemData.cid,""+measurement_id,""+bill_id,"tailor",edt_amt.getText().toString()));
                }
                Toast.makeText(getActivity().getApplicationContext(),"Done",Toast.LENGTH_LONG).show();
                //txt_val.setText(dt);

            }
        });
        return view;
    }

    public static void setData() {
        tbl_layout.removeAllViews();
        final TableRow tableRow = new TableRow(context);
        tableRow.setGravity(Gravity.CENTER);
        tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));
        //tableRow.setBackground(getResources().getDrawable(R.drawable.border));
        for (int i = 0; i < OrderItemData.ldata.size() + 2; i++) {
            // Add a TextView in the first column.
            if (i == 0) {
                TextView textView = new TextView(context);
                textView.setText("Measurs");
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                //textView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
                textView.setLayoutParams(new TableRow.LayoutParams(100, 50));
                textView.setTextSize(12f);
                tableRow.addView(textView, i);
            } else if (i == 1) {
                TextView textView = new TextView(context);
                textView.setText("common");
                textView.setPadding(5, 5, 5, 5);
                textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                textView.setTextSize(12f);
                tableRow.addView(textView, i);
            } else {
                spc.add("");
                cmts.add("");
                final TextView textView = new TextView(context);
                if ((OrderItemData.ldata.get(i-2).getIname().equalsIgnoreCase("Tuxido 3.Pc") || OrderItemData.ldata.get(i-2).getIname().equalsIgnoreCase("Basic S.B 3 Pc"))&&flg==1){
                    iflg = i;
                    textView.setText("Pant");
                    textView.setPadding(5, 5, 5, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                    textView.setTextSize(12f);
                    final int itemp = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SpecificationDialog dialog = new SpecificationDialog(OrderItemData.ldata.get(itemp-2).getIspecification(),itemp-2,"lower");
                            dialog.show(fm,"load");
                            Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                    tableRow.addView(textView, i);
                    flg=0;
                }
                else if ((OrderItemData.ldata.get(i-2).getIname().equalsIgnoreCase("Tuxido 2.Pc") || OrderItemData.ldata.get(i-2).getIname().equalsIgnoreCase("Basic S.B 2 Pc"))&&flg1==1){
                    iflg = i;
                    textView.setText("Pant");
                    textView.setPadding(5, 5, 5, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                    textView.setTextSize(12f);
                    final int itemp = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SpecificationDialog dialog = new SpecificationDialog(OrderItemData.ldata.get(itemp-2).getIspecification(),itemp-2,"lower");
                            dialog.show(fm,"load");
                            Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                    tableRow.addView(textView, i);
                    flg1=0;
                }
                else {
                    textView.setText(OrderItemData.ldata.get(i - 2).getIname());
                    textView.setPadding(5, 5, 5, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                    textView.setTextSize(12f);
                    final int itemp = i;
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SpecificationDialog dialog = new SpecificationDialog(OrderItemData.ldata.get(itemp-2).getIspecification(),itemp-2,"lower");
                            dialog.show(fm,"load");
                            Toast.makeText(context,textView.getText().toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                    tableRow.addView(textView, i);
                }
            }
        }
        tbl_layout.addView(tableRow);
        for (int i = 1; i < measurements.length; i++) {
            TableRow tableRow1 = new TableRow(context);
            tableRow1.setGravity(Gravity.CENTER);
            tableRow1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));

            for (int j = 0; j < OrderItemData.ldata.size() + cls; j++) {
                // Add a TextView in the first column.
                if (j == 0) {
                    TextView textView = new TextView(context);
                    textView.setText(measurements[i-1]);
                    textView.setPadding(15, 5, 15, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    textView.setLayoutParams(new TableRow.LayoutParams(100, 50));
                    textView.setTextSize(12f);
                    tableRow1.addView(textView, j);
                } else {

                    AutoCompleteTextView textView = new AutoCompleteTextView(context);
                    //textView.setHint(args[j]);
                    textView.setPadding(5, 5, 5, 5);
                    textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setHintTextColor(context.getResources().getColor(R.color.colorAccent));
                    textView.setBackground(context.getResources().getDrawable(R.drawable.border));
                    SizeSearchAdapter adapter = new SizeSearchAdapter(context, R.layout.fragment_upper_measurement, R.id.txt_mob,data);
                    textView.setAdapter(adapter);
                    textView.setInputType(InputType.TYPE_CLASS_NUMBER);
                    textView.setLayoutParams(new TableRow.LayoutParams(90, 50));
                    textView.setTextSize(12f);
                    textView.setGravity(Gravity.CENTER_HORIZONTAL);
                    if (OrderItemData.lmeasurementsList.size()>0){
                        //OrderItemData.lmeasurementsList.get(i-2);
                        try {
                            if (j==1){
                                switch (i) {
                                    case 1:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 2:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 3:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 4:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 5:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 6:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 7:
                                        textView.setText(common.get(i - 1));
                                        break;
                                    case 8:
                                        textView.setText(common.get(i - 1));
                                        break;

                                }
                            }
                            else {
                                switch (i) {
                                    case 1:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getLength()))
                                            textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getLength());
                                        break;
                                    case 2:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getWaist()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getWaist());
                                        break;
                                    case 3:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getHip()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getHip());
                                        break;
                                    case 4:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getThie()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getThie());
                                        break;
                                    case 5:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getKnee()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getKnee());
                                        break;
                                    case 6:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getKnee2()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getKnee2());
                                        break;
                                    case 7:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getBottom()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getBottom());
                                        break;
                                    case 8:
                                        if (!common.get(i-1).equalsIgnoreCase(OrderItemData.lmeasurementsList.get(j-cls).getFolk()))
                                        textView.setText(OrderItemData.lmeasurementsList.get(j - cls).getFolk());
                                        break;

                                }
                            }
                        }catch (Exception e){
                            Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                    tableRow1.addView(textView, j);
                }

            }
            tbl_layout.addView(tableRow1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (flg==0)
            flg=1;
        setData();

    }
}
