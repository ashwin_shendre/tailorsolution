package com.tailorsolutions.com.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.common.SharedPreferenceDb;
import com.tailorsolutions.com.tailoringsolutions.R;

@SuppressLint("ValidFragment")
public class LowerMeasurementFragment extends DialogFragment {
    private SharedPreferences spf;
    EditText txt_len,txt_waist,txt_hip,txt_thie,txt_knee,txt_bottom,txt_folk,txt_feta,txt_mouzari;
    TextView txt_save;
    String iname;
    @SuppressLint("ValidFragment")
    public  LowerMeasurementFragment(String iname){
        this.iname = iname;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frament_lower_measurement,container,false);
        /*txt_len = (EditText) v.findViewById(R.id.edt_len);
        txt_waist = (EditText) v.findViewById(R.id.edt_waist);
        txt_hip = (EditText) v.findViewById(R.id.edt_hip);
        txt_thie = (EditText) v.findViewById(R.id.edt_thie);
        txt_knee = (EditText) v.findViewById(R.id.edt_knee);
        txt_bottom = (EditText) v.findViewById(R.id.edt_bottom);
        txt_folk = (EditText) v.findViewById(R.id.edt_folk);
        txt_feta = (EditText) v.findViewById(R.id.edt_feta);
        txt_mouzari = (EditText) v.findViewById(R.id.edt_mouzari);
        txt_save = (TextView) v.findViewById(R.id.tvsave);
        find();
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spf = SharedPreferenceDb.getMeasureLowerInstanace(getActivity().getApplicationContext());
                SharedPreferences.Editor editor = spf.edit();
                editor.putString("lower",iname);
                editor.putString("type","upper");
                editor.putString("length",txt_len.getText().toString());
                editor.putString("waist",txt_waist.getText().toString());
                editor.putString("hip",txt_hip.getText().toString());
                editor.putString("thie",txt_thie.getText().toString());
                editor.putString("knee",txt_knee.getText().toString());
                editor.putString("bottom",txt_bottom.getText().toString());
                editor.putString("folk",txt_folk.getText().toString());
                editor.putString("feta",txt_feta.getText().toString());
                editor.putString("mouzari",txt_mouzari.getText().toString());
                editor.commit();
                Toast.makeText(getActivity().getApplicationContext(),"Measurement Stored",Toast.LENGTH_LONG).show();
                dismiss();
            }
        });*/

        return v;
    }

    private void find() {
        spf = getActivity().getApplicationContext().getSharedPreferences(iname, Context.MODE_PRIVATE);

        if (spf!=null){
            String utype =spf.getString("lower",null);
            if (utype!=null && utype.equalsIgnoreCase(iname)) {
                txt_len.setText(spf.getString("length", null));
                txt_waist.setText(spf.getString("waist", null));
                txt_hip.setText(spf.getString("hip", null));
                txt_thie.setText(spf.getString("thie", null));
                txt_knee.setText(spf.getString("knee", null));
                txt_bottom.setText(spf.getString("bottom", null));
                txt_folk.setText(spf.getString("folk", null));
                txt_feta.setText(spf.getString("feta", null));
                txt_mouzari.setText(spf.getString("mouzari", null));
            }
        }
    }
}
