package com.tailorsolutions.com.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tailorsolutions.com.adapter.EmployeeAdapter;
import com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity;
import com.tailorsolutions.com.tailoringsolutions.R;
import com.tailorsolutions.com.utils.Bill;
import com.tailorsolutions.com.utils.Measurement;
import com.tailorsolutions.com.utils.OrderItemData;
import com.tailorsolutions.com.utils.Orders;

import java.util.List;

import static com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity.bill_id;
import static com.tailorsolutions.com.tailoringsolutions.NewOrderPageActivity.billrepository;

public class OrderPageFragment extends Fragment {

    View view;
    EditText edt_paid,edt_total,edt_pending;
    public static EmployeeAdapter adapter;
    public static ListView lst_orders;
    public static Context context;
    TextView txt_save;
    public static Activity activity;
    public static String bpaid,btotal,bpending;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order_page,container,false);
        TextView btn_click = (TextView) view.findViewById(R.id.btn_add);
        txt_save = (TextView) view.findViewById(R.id.btn_find);
        /*edt_paid = (EditText) view.findViewById(R.id.edt_paid);
        edt_total = (EditText) view.findViewById(R.id.edt_total);
        edt_pending = (EditText) view.findViewById(R.id.edt_payabel);*/
        context = getActivity().getApplicationContext();
        activity = getActivity();
        /*edt_paid.setVisibility(View.INVISIBLE);
        edt_total.setVisibility(View.INVISIBLE);
        edt_pending.setVisibility(View.INVISIBLE);*/
        adapter = new EmployeeAdapter(getActivity().getApplicationContext(),getFragmentManager());
        lst_orders = (ListView) view.findViewById(R.id.lst_items);
        //lst_orders.setAdapter(adapter);
        //OrderItemData.udata.clear();
        /*OrderItemData.udata.add("Measures");
        OrderItemData.udata.add("Common");
        OrderItemData.ldata.clear();
        OrderItemData.ldata.add("Measures");
        OrderItemData.ldata.add("Common");*/
        btn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpperMeasurementFragment dialog = new UpperMeasurementFragment();
                dialog.show(getFragmentManager(),"Upper Measurement");
            }
        });
        txt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AmountDialog dialog = new AmountDialog();
                dialog.show(getFragmentManager(),"Amount");
                //getActivity().finish();
            }
        });//Amruta pandit 8208048146(about Project) Archana Kulkarni-8237208617(about Experience.)
        /*edt_paid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length()>0) {
                    int total = Integer.parseInt(edt_total.getText().toString());
                    int paid = Integer.parseInt(s.toString());
                    edt_pending.setText("" + (total - paid));
                }
            }
        });*/
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (OrderItemData.data.size()>0){
            adapter.setData(OrderItemData.data);
            lst_orders.setAdapter(adapter);
        }
    }
    public static void fetchVal(){
        Toast.makeText(context,"Bill amount="+btotal+" "+bpaid+" "+bpending,Toast.LENGTH_LONG).show();
    }
    public static void stroeData(){
        List<Measurement> umymeasures = OrderItemData.umeasurementsList;
        List<Measurement> lmymeasures = OrderItemData.lmeasurementsList;
        List<OrderItemData> dt = OrderItemData.data;
        int uindex=0,lindex=0;
        billrepository.insert(new Bill(btotal,bpaid,bpending,OrderItemData.cid));
        for(int i=0;i<dt.size();i++){
            if (dt.get(i).getItem().getItype().equalsIgnoreCase("upper")){
                NewOrderPageActivity.measurementrepository.insert(OrderItemData.umeasurementsList.get(uindex));
                NewOrderPageActivity.measurementid++;
                OrderItemData.t3ms = ""+NewOrderPageActivity.measurementid;
                Toast.makeText(context,""+ NewOrderPageActivity.measurementid,Toast.LENGTH_LONG).show();
                uindex++;
            }
            else if (dt.get(i).getItem().getItype().equalsIgnoreCase("lower")){
                NewOrderPageActivity.measurementrepository.insert(OrderItemData.lmeasurementsList.get(lindex));
                NewOrderPageActivity.measurementid++;
                OrderItemData.t3ms = ""+NewOrderPageActivity.measurementid;
                //if (i==Fra)
                if (i==0)
                    OrderItemData.t3ms=""+NewOrderPageActivity.measurementid;
                OrderItemData.t3ms=OrderItemData.t3ms+","+NewOrderPageActivity.measurementid;
                Toast.makeText(context,""+ NewOrderPageActivity.measurementid,Toast.LENGTH_LONG).show();
                lindex++;
            }
            else{
                for (int t =0;t<OrderItemData.tmeasurementsList.size();t++){
                    NewOrderPageActivity.measurementrepository.insert(OrderItemData.tmeasurementsList.get(t));
                    NewOrderPageActivity.measurementid++;
                    if (t==0)
                        OrderItemData.t3ms=""+NewOrderPageActivity.measurementid;
                    else
                        OrderItemData.t3ms=OrderItemData.t3ms+","+NewOrderPageActivity.measurementid;
                }
            }

            NewOrderPageActivity.orderrepository.insert(new Orders(""+OrderItemData.data.get(i).getItem().getId(),OrderItemData.data.get(i).getItem().getIname(),OrderItemData.data.get(i).qty,OrderItemData.cid,OrderItemData.t3ms,""+bill_id,"tailor",""));
        }
        activity.finish();
    }
}
