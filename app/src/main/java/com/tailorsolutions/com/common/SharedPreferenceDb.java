package com.tailorsolutions.com.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Display;

public class SharedPreferenceDb {
    private static final String SHARED_PREF_NAME = "user_info";
    private static final String MEASUREMENT_UPPER = "upper_measurement";
    private static final String MEASUREMENT_LOWER = "lower_measurement";
    public static final String KEY_NAME = "key_username";
    public static final String KEY_PASSWORD = "key_password";
    private static SharedPreferences spf;
    private static SharedPreferences.Editor editor;

    private SharedPreferenceDb(){}
    public static SharedPreferences getInstance(Context context){
        if(spf == null){
            spf = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
       }
       return spf;
    }
    public static SharedPreferences getMeasureUpperInstanace(Context context){
        spf = context.getSharedPreferences(MEASUREMENT_UPPER,Context.MODE_PRIVATE);
        return spf;
    }
    public static SharedPreferences getMeasureLowerInstanace(Context context){
        spf = context.getSharedPreferences(MEASUREMENT_LOWER,Context.MODE_PRIVATE);
        return spf;
    }
    public static SharedPreferences.Editor getEditor(){
        if(editor == null){
            editor = spf.edit();
        }
        return editor;
    }
}
